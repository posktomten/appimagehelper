<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>Categories</name>
    <message>
        <location filename="../categories.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../categories.ui" line="20"/>
        <source>TextLabel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../categories.ui" line="32"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../categories.ui" line="39"/>
        <source>Exit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../categories.cpp" line="40"/>
        <source>Application for presenting, creating, or processing multimedia (audio/video).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../categories.cpp" line="42"/>
        <source>An audio application.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../categories.cpp" line="42"/>
        <source>A video application.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../categories.cpp" line="43"/>
        <source>An application for development.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../categories.cpp" line="43"/>
        <source>Educational software.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../categories.cpp" line="44"/>
        <source>A game.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../categories.cpp" line="45"/>
        <source>Application for viewing, creating, or processing graphics.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../categories.cpp" line="46"/>
        <source>Network application such as a web browser.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../categories.cpp" line="47"/>
        <source>An office type application.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../categories.cpp" line="47"/>
        <source>Scientific software.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../categories.cpp" line="48"/>
        <source>Settings applications.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../categories.cpp" line="49"/>
        <source>System application, &quot;System Tools&quot; such as say a log viewer or network monitor.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../categories.cpp" line="51"/>
        <source>Small utility application, &quot;Accessories&quot;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../categories.cpp" line="85"/>
        <source>You must select at least one category.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../categories.cpp" line="100"/>
        <source>These settings apply to all *.desktop files in line </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../categories.cpp" line="100"/>
        <source>Categories for</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Dialog</name>
    <message>
        <location filename="../dialog.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="30"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="37"/>
        <source>Exit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="48"/>
        <source>Failure!
Could not open file</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Info</name>
    <message>
        <location filename="../info.cpp" line="29"/>
        <source> is free software, license </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../info.cpp" line="30"/>
        <source>appImageHelper is a program for creating, deleting, controlling and organizing shortcuts to AppImage.&lt;br&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../info.cpp" line="37"/>
        <source> was created </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../info.cpp" line="38"/>
        <source>by a computer with</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../info.cpp" line="51"/>
        <location filename="../info.cpp" line="60"/>
        <location filename="../info.cpp" line="69"/>
        <source> Compiled by</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../info.cpp" line="79"/>
        <source>Unknown compiler.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../info.cpp" line="91"/>
        <location filename="../info.cpp" line="108"/>
        <source>Home page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../info.cpp" line="93"/>
        <location filename="../info.cpp" line="110"/>
        <source>Source code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../info.cpp" line="95"/>
        <location filename="../info.cpp" line="112"/>
        <source>Wiki</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../info.cpp" line="98"/>
        <location filename="../info.cpp" line="115"/>
        <source>Phone: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../info.cpp" line="99"/>
        <location filename="../info.cpp" line="116"/>
        <source>About </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../info.cpp" line="103"/>
        <location filename="../info.cpp" line="120"/>
        <source>This program uses </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../info.cpp" line="103"/>
        <location filename="../info.cpp" line="120"/>
        <source> version </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../info.cpp" line="104"/>
        <location filename="../info.cpp" line="121"/>
        <source> running on </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Widget</name>
    <message>
        <location filename="../widget.ui" line="14"/>
        <source>Widget</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget.ui" line="24"/>
        <source>Decide the location of the shortcut(s).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget.ui" line="31"/>
        <source>Symbolic link (ln -s) in any folder.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget.ui" line="44"/>
        <source>Shortcut on the desktop (~/Desktop).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget.ui" line="68"/>
        <source>Use Standard *.desktop file (Recommended).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget.ui" line="87"/>
        <source>Use Symbolic link (ln -s).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget.ui" line="99"/>
        <source>Decide the category for your AppImage.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget.ui" line="176"/>
        <source>Search for Update</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget.ui" line="186"/>
        <source>Update</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget.ui" line="193"/>
        <location filename="../widget.cpp" line="225"/>
        <source>Launch AppImage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget.ui" line="224"/>
        <source>Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget.ui" line="252"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget.ui" line="283"/>
        <source>Exit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget.ui" line="311"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Remove missing shortcuts from the database. Red background color.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget.ui" line="314"/>
        <source>Remove missing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget.ui" line="359"/>
        <source>Check your shortcuts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget.ui" line="390"/>
        <source>Create Shortcut</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget.ui" line="418"/>
        <source>Delete selected shortcuts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget.ui" line="428"/>
        <source>Resize to Content</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget.ui" line="435"/>
        <source>No horizontal scrollbar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget.ui" line="454"/>
        <source>View Path.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget.ui" line="473"/>
        <source>Do not check for a new version when the program starts.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../addshortcut.cpp" line="42"/>
        <source>Select icon file (Not required</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../addshortcut.cpp" line="43"/>
        <source>Icon (*.ico *.png *.jpg *.jepg *.svg)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../addshortcut.cpp" line="123"/>
        <source>Select Category</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appimage.cpp" line="32"/>
        <source>Select AppImage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appimage.cpp" line="33"/>
        <source>AppImage (*.AppImage)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appimage.cpp" line="42"/>
        <source>AppImage does not exist.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appimage.cpp" line="58"/>
        <location filename="../makedesktopfile.cpp" line="47"/>
        <location filename="../rewritedesktopfile.cpp" line="36"/>
        <location filename="../rewritedesktopfile.cpp" line="71"/>
        <location filename="../rewritedesktopfile.cpp" line="95"/>
        <source>Failure!
The shortcut could not be created.
Check your file permissions.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../config.cpp" line="84"/>
        <location filename="../widget.cpp" line="421"/>
        <source>Select &quot;Update&quot; to update</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../editdesktop.cpp" line="49"/>
        <location filename="../editdesktop.cpp" line="124"/>
        <source>Failed to create .desktop file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../editdesktop.cpp" line="76"/>
        <source>Failed to edit *.desktop file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../editdesktop.cpp" line="149"/>
        <source>Failed to create *.desktop file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../icon.cpp" line="48"/>
        <source>Select icon file (Not required) png, xpm and svg are standard formats</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../icon.cpp" line="49"/>
        <source>Icon (*.png *.xpm *.svg *.ico *.jpg *.jepg)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../symlink.cpp" line="29"/>
        <location filename="../symlink.cpp" line="84"/>
        <source>Select folder for the symbolic link</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../symlink.cpp" line="41"/>
        <location filename="../symlink.cpp" line="96"/>
        <location filename="../symlink.cpp" line="106"/>
        <source>Failure!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../symlink.cpp" line="42"/>
        <location filename="../symlink.cpp" line="97"/>
        <location filename="../symlink.cpp" line="107"/>
        <source>You do not have the right to create a link in this folder.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../symlink.cpp" line="60"/>
        <location filename="../symlink.cpp" line="68"/>
        <location filename="../widget.cpp" line="822"/>
        <source>An unexpected error occurred while creating the symbolic link.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../symlink.cpp" line="121"/>
        <location filename="../symlink.cpp" line="129"/>
        <source>An unexpected error occurred while creating the symbolic link</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../tableheader.cpp" line="28"/>
        <source>Created</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../tableheader.cpp" line="29"/>
        <source>Path to AppImage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../tableheader.cpp" line="30"/>
        <source>Path to Icon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../tableheader.cpp" line="31"/>
        <source>Symbolic link</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../tableheader.cpp" line="32"/>
        <location filename="../tableheader.cpp" line="39"/>
        <source>*.desktop file in:
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../tableheader.cpp" line="33"/>
        <source>Symbolic link in:
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="34"/>
        <source>*.desktop file in: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="57"/>
        <location filename="../widget.cpp" line="66"/>
        <source>
(Recommended).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="79"/>
        <source>Delete all settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="80"/>
        <source>Force update</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="99"/>
        <location filename="../widget.cpp" line="126"/>
        <source>There is no configuration file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="105"/>
        <source>All configuration files will be moved to the Trash and the program will close.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="106"/>
        <source>All shortcuts created by </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="106"/>
        <source> will remain.
Do you want to continue?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="119"/>
        <location filename="../widget.cpp" line="125"/>
        <source>Failed to move the configuration file to the Trash.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="120"/>
        <source>Unexpected error.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="167"/>
        <source>Copy Path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="195"/>
        <source>Make the AppImage Executable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="219"/>
        <source>Could not make the AppImage executable.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="252"/>
        <source>Add Icon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="255"/>
        <source>Add Symbolic Link</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="258"/>
        <source>Add *.desktop file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="266"/>
        <source>Unable to add a shortcut</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="267"/>
        <source>There is already a shortcut here. You must remove it before you can create a new one.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="300"/>
        <source>Remove AppImage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="303"/>
        <source>Remove Icon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="306"/>
        <source>Remove Symbolic Link</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="309"/>
        <source>Remove the *.desktop file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="328"/>
        <source>Edit Categories</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="362"/>
        <source>View the *.desktop file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="517"/>
        <source>Application for presenting, creating, or processing multimedia (audio/video).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="519"/>
        <source>An audio application.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="519"/>
        <source>A video application.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="520"/>
        <source>An application for development.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="520"/>
        <source>Educational software.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="521"/>
        <source>A game.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="522"/>
        <source>Application for viewing, creating, or processing graphics.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="523"/>
        <source>Network application such as a web browser.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="524"/>
        <source>An office type application.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="524"/>
        <source>Scientific software.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="525"/>
        <source>Settings applications.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="526"/>
        <source>System application, &quot;System Tools&quot; such as say a log viewer or network monitor.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="528"/>
        <source>Small utility application, &quot;Accessories&quot;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="615"/>
        <source>must be restarted for the new language settings to take effect.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="831"/>
        <source>Icon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="851"/>
        <source>Not executable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="864"/>
        <source>Missing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="867"/>
        <source>Missing!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CheckUpdate</name>
    <message>
        <location filename="../checkupdate.cpp" line="35"/>
        <location filename="../checkupdate.cpp" line="51"/>
        <location filename="../checkupdate.cpp" line="75"/>
        <location filename="../checkupdate.cpp" line="93"/>
        <location filename="../checkupdate.cpp" line="102"/>
        <location filename="../checkupdate.cpp" line="111"/>
        <location filename="../checkupdate.cpp" line="143"/>
        <location filename="../checkupdate.cpp" line="178"/>
        <location filename="../checkupdate.cpp" line="251"/>
        <location filename="../checkupdate.cpp" line="260"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="53"/>
        <location filename="../checkupdate.cpp" line="74"/>
        <location filename="../checkupdate.cpp" line="145"/>
        <location filename="../checkupdate.cpp" line="259"/>
        <source>No Internet connection was found.
Please check your Internet settings and firewall.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="91"/>
        <source>Your version of </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="92"/>
        <source> is newer than the latest official version. </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="101"/>
        <source>You have the latest version of </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="110"/>
        <source>There was an error when the version was checked.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="177"/>
        <source>
There was an error when the version was checked.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="222"/>
        <source>Updates:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="245"/>
        <source>There is a new version of </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="247"/>
        <source>Latest version: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="29"/>
        <source>No error.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="33"/>
        <source>The remote server closed the connection prematurely, before the entire reply was received and processed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="37"/>
        <source>The remote host name was not found (invalid hostname).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="41"/>
        <source>The connection to the remote server timed out.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="45"/>
        <source>The operation was canceled via calls to abort() or close() before it was finished.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="49"/>
        <source>The SSL/TLS handshake failed and the encrypted channel could not be established. The sslErrors() signal should have been emitted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="53"/>
        <source>The connection was broken due to disconnection from the network, however the system has initiated roaming to another access point. The request should be resubmitted and will be processed as soon as the connection is re-established.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="57"/>
        <source>The connection was broken due to disconnection from the network or failure to start the network.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="61"/>
        <source>The background request is not currently allowed due to platform policy.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="65"/>
        <source>While following redirects, the maximum limit was reached. The limit is by default set to 50 or as set by QNetworkRequest::setMaxRedirectsAllowed().</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="69"/>
        <source>While following redirects, the network access API detected a redirect from a encrypted protocol (https) to an unencrypted one (http).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="73"/>
        <source>An unknown network-related error was detected.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="77"/>
        <source>The connection to the proxy server was refused (the proxy server is not accepting requests).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="81"/>
        <source>The proxy server closed the connection prematurely, before the entire reply was received and processed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="85"/>
        <source>The proxy host name was not found (invalid proxy hostname).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="89"/>
        <source>The connection to the proxy timed out or the proxy did not reply in time to the request sent.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="93"/>
        <source>The proxy requires authentication in order to honour the request but did not accept any credentials offered (if any).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="97"/>
        <source>An unknown proxy-related error was detected.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="101"/>
        <source>The access to the remote content was denied (similar to HTTP error 403).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="105"/>
        <source>The operation requested on the remote content is not permitted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="109"/>
        <source>The remote content was not found at the server (similar to HTTP error 404).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="113"/>
        <source>The remote server requires authentication to serve the content but the credentials provided were not accepted (if any).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="117"/>
        <source>The request needed to be sent again, but this failed for example because the upload data could not be read a second time.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="121"/>
        <source>The request could not be completed due to a conflict with the current state of the resource.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="125"/>
        <source>The requested resource is no longer available at the server.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="129"/>
        <source>An unknown error related to the remote content was detected.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="133"/>
        <source>The Network Access API cannot honor the request because the protocol is not known.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="137"/>
        <source>The requested operation is invalid for this protocol.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="141"/>
        <source>A breakdown in protocol was detected (parsing error, invalid or unexpected responses, etc.).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="145"/>
        <source>The server encountered an unexpected condition which prevented it from fulfilling the request.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="149"/>
        <source>The server does not support the functionality required to fulfill the request.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="153"/>
        <source>The server is unable to handle the request at this time.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="157"/>
        <source>Unknown Server Error.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="161"/>
        <source>The remote server refused the connection (the server is not accepting requests).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="165"/>
        <source>Unknown Error.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Update</name>
    <message>
        <location filename="../update.cpp" line="34"/>
        <location filename="../update.cpp" line="67"/>
        <source>Update</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../update.cpp" line="35"/>
        <source>zsync cannot be found in path:
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../update.cpp" line="36"/>
        <source>Unable to update.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../update.cpp" line="68"/>
        <source>An unexpected error occurred. Error message:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../update.cpp" line="78"/>
        <source>is updated.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../update.cpp" line="79"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UpdateDialog</name>
    <message>
        <location filename="../updatedialog.ui" line="20"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../updatedialog.cpp" line="31"/>
        <source>Update</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../updatedialog.cpp" line="47"/>
        <source>Updating, please wait...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
