<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sv_SE" sourcelanguage="en_US">
<context>
    <name>Categories</name>
    <message>
        <location filename="../categories.ui" line="14"/>
        <source>Form</source>
        <translation>Form</translation>
    </message>
    <message>
        <location filename="../categories.ui" line="20"/>
        <source>TextLabel</source>
        <translation>TextLabel</translation>
    </message>
    <message>
        <location filename="../categories.ui" line="32"/>
        <source>Save</source>
        <translation>Spara</translation>
    </message>
    <message>
        <location filename="../categories.ui" line="39"/>
        <source>Exit</source>
        <translation>Avsluta</translation>
    </message>
    <message>
        <location filename="../categories.cpp" line="40"/>
        <source>Application for presenting, creating, or processing multimedia (audio/video).</source>
        <translation>Program för att presentera, skapa eller bearbeta multimedia (ljud / video).</translation>
    </message>
    <message>
        <location filename="../categories.cpp" line="42"/>
        <source>An audio application.</source>
        <translation>En ljudapplikation.</translation>
    </message>
    <message>
        <location filename="../categories.cpp" line="42"/>
        <source>A video application.</source>
        <translation>En videoapplikation.</translation>
    </message>
    <message>
        <location filename="../categories.cpp" line="43"/>
        <source>An application for development.</source>
        <translation>En applikation för utveckling.</translation>
    </message>
    <message>
        <location filename="../categories.cpp" line="43"/>
        <source>Educational software.</source>
        <translation>Utbildningsprogramvara.</translation>
    </message>
    <message>
        <location filename="../categories.cpp" line="44"/>
        <source>A game.</source>
        <translation>Ett spel.</translation>
    </message>
    <message>
        <location filename="../categories.cpp" line="45"/>
        <source>Application for viewing, creating, or processing graphics.</source>
        <translation>Applikation för visning, skapande eller bearbetning av grafik.</translation>
    </message>
    <message>
        <location filename="../categories.cpp" line="46"/>
        <source>Network application such as a web browser.</source>
        <translation>Nätverksapplikation som till exempel en webbläsare.</translation>
    </message>
    <message>
        <location filename="../categories.cpp" line="47"/>
        <source>An office type application.</source>
        <translation>Ett kontorsprogram.</translation>
    </message>
    <message>
        <location filename="../categories.cpp" line="47"/>
        <source>Scientific software.</source>
        <translation>Vetenskaplig programvara.</translation>
    </message>
    <message>
        <location filename="../categories.cpp" line="48"/>
        <source>Settings applications.</source>
        <translation>Program för att göra inställningar.</translation>
    </message>
    <message>
        <location filename="../categories.cpp" line="49"/>
        <source>System application, &quot;System Tools&quot; such as say a log viewer or network monitor.</source>
        <translation>Systemapplikation, &quot;Systemverktyg&quot; som t.ex. en loggvisare eller nätverksmonitor.</translation>
    </message>
    <message>
        <location filename="../categories.cpp" line="51"/>
        <source>Small utility application, &quot;Accessories&quot;.</source>
        <translation>Litet verktygsprogram, &quot;Tillbehör&quot;.</translation>
    </message>
    <message>
        <location filename="../categories.cpp" line="85"/>
        <source>You must select at least one category.</source>
        <translation>Du måste välja minst en kategori.</translation>
    </message>
    <message>
        <location filename="../categories.cpp" line="100"/>
        <source>These settings apply to all *.desktop files in line </source>
        <translation>Dessa inställningar gäller alla *.desktop-filer på rad </translation>
    </message>
    <message>
        <location filename="../categories.cpp" line="100"/>
        <source>Categories for</source>
        <translation>Kategorier för</translation>
    </message>
</context>
<context>
    <name>Dialog</name>
    <message>
        <location filename="../dialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="30"/>
        <source>Save</source>
        <translation>Spara</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="37"/>
        <source>Exit</source>
        <translation>Avsluta</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="48"/>
        <source>Failure!
Could not open file</source>
        <translation>Fel!
Kunde inte öppna filen</translation>
    </message>
</context>
<context>
    <name>Info</name>
    <message>
        <location filename="../info.cpp" line="29"/>
        <source> is free software, license </source>
        <translation> är fri mjukvara, license </translation>
    </message>
    <message>
        <location filename="../info.cpp" line="30"/>
        <source>appImageHelper is a program for creating, deleting, controlling and organizing shortcuts to AppImage.&lt;br&gt;</source>
        <translation>appImageHelper är ett program för att skapa, ta bort, kontrollera och organisera genvägar till AppImage. &lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="37"/>
        <source> was created </source>
        <translation> skapades </translation>
    </message>
    <message>
        <location filename="../info.cpp" line="38"/>
        <source>by a computer with</source>
        <translation>av en dator med</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="51"/>
        <location filename="../info.cpp" line="60"/>
        <location filename="../info.cpp" line="69"/>
        <source> Compiled by</source>
        <translation> Kompilerad av</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="79"/>
        <source>Unknown compiler.</source>
        <translation>Okänd kompilerare.</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="91"/>
        <location filename="../info.cpp" line="108"/>
        <source>Home page</source>
        <translation>Hemsida</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="93"/>
        <location filename="../info.cpp" line="110"/>
        <source>Source code</source>
        <translation>källkod</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="95"/>
        <location filename="../info.cpp" line="112"/>
        <source>Wiki</source>
        <translation>Wiki</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="98"/>
        <location filename="../info.cpp" line="115"/>
        <source>Phone: </source>
        <translation>Telefon: </translation>
    </message>
    <message>
        <location filename="../info.cpp" line="99"/>
        <location filename="../info.cpp" line="116"/>
        <source>About </source>
        <translation>Om </translation>
    </message>
    <message>
        <location filename="../info.cpp" line="103"/>
        <location filename="../info.cpp" line="120"/>
        <source> version </source>
        <translation> version </translation>
    </message>
    <message>
        <location filename="../info.cpp" line="104"/>
        <location filename="../info.cpp" line="121"/>
        <source> running on </source>
        <translation> som körs på </translation>
    </message>
    <message>
        <location filename="../info.cpp" line="103"/>
        <location filename="../info.cpp" line="120"/>
        <source>This program uses </source>
        <translation>Det här programmet använder </translation>
    </message>
</context>
<context>
    <name>Widget</name>
    <message>
        <location filename="../widget.ui" line="14"/>
        <source>Widget</source>
        <translation>Widget</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="24"/>
        <source>Decide the location of the shortcut(s).</source>
        <translation>Bestäm plats för genvägarna.</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="31"/>
        <source>Symbolic link (ln -s) in any folder.</source>
        <translation>Symbolisk länk (ln -s) i valfri mapp.</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="44"/>
        <source>Shortcut on the desktop (~/Desktop).</source>
        <translation>Genväg på skrivbordet (~/Desktop).</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="87"/>
        <source>Use Symbolic link (ln -s).</source>
        <translation>Använd symbolisk länk (ln -s).</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="99"/>
        <source>Decide the category for your AppImage.</source>
        <translation>Bestäm kategorin för din AppImage.</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="176"/>
        <source>Search for Update</source>
        <translation>Sök efter uppdateringar</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="186"/>
        <source>Update</source>
        <translation>Uppdatera</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="193"/>
        <location filename="../widget.cpp" line="225"/>
        <source>Launch AppImage</source>
        <translation>Starta AppImage</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="224"/>
        <source>Help</source>
        <translation>Hjälp</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="252"/>
        <source>About</source>
        <translation>Om</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="311"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Remove missing shortcuts from the database. Red background color.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Ta bort saknade genvägar från databasen. Röd bakgrundsfärg.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="314"/>
        <source>Remove missing</source>
        <translation>Ta bort saknade</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="359"/>
        <source>Check your shortcuts</source>
        <translation>Kolla dina genvägar</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="418"/>
        <source>Delete selected shortcuts</source>
        <translation>Radera valda genvägar</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="428"/>
        <source>Resize to Content</source>
        <translation>Anpassa storleken till innehållet</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="435"/>
        <source>No horizontal scrollbar</source>
        <translation>Ingen vågrät rullningslist</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="454"/>
        <source>View Path.</source>
        <translation>Visa sökväg.</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="473"/>
        <source>Do not check for a new version when the program starts.</source>
        <translation>Sök inte efter en ny version när programmet startar.</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="390"/>
        <source>Create Shortcut</source>
        <translation>Skapa genväg</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="68"/>
        <source>Use Standard *.desktop file (Recommended).</source>
        <translation>Använd Standard *.desktop-fil (Rekommenderas).</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="283"/>
        <source>Exit</source>
        <translation>Avsluta</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="34"/>
        <source>*.desktop file in: </source>
        <translation>*.desktop fil i: </translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="57"/>
        <location filename="../widget.cpp" line="66"/>
        <source>
(Recommended).</source>
        <translation>
(Rekommenderas).</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="195"/>
        <source>Make the AppImage Executable</source>
        <translation>Gör AppImagen körbar</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="219"/>
        <source>Could not make the AppImage executable.</source>
        <translation>Kunde inte göra AppImagen körbar.</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="266"/>
        <source>Unable to add a shortcut</source>
        <translation>Det går inte att lägga till en genväg</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="267"/>
        <source>There is already a shortcut here. You must remove it before you can create a new one.</source>
        <translation>Det finns redan en genväg här. Du måste ta bort den innan du kan skapa en ny.</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="167"/>
        <source>Copy Path</source>
        <translation>Kopiera sökvägen</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="328"/>
        <source>Edit Categories</source>
        <translation>Redigera kategorier</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="252"/>
        <source>Add Icon</source>
        <translation>Lägg till ikon</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="79"/>
        <source>Delete all settings</source>
        <translation>Radera alla inställningar</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="80"/>
        <source>Force update</source>
        <translation>Tvinga uppdatering</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="99"/>
        <location filename="../widget.cpp" line="126"/>
        <source>There is no configuration file.</source>
        <translation>Det finns ingen konfigurationsfil.</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="105"/>
        <source>All configuration files will be moved to the Trash and the program will close.</source>
        <translation>Alla konfigurationsfiler flyttas till papperskorgen och programmet stängs.</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="106"/>
        <source>All shortcuts created by </source>
        <translation>Alla genvägar skapade av </translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="106"/>
        <source> will remain.
Do you want to continue?</source>
        <translation> kommer att finnas kvar.
Vill du fortsätta?</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="119"/>
        <location filename="../widget.cpp" line="125"/>
        <source>Failed to move the configuration file to the Trash.</source>
        <translation>Det gick inte att flytta konfigurationsfilen till papperskorgen.</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="120"/>
        <source>Unexpected error.</source>
        <translation>Oväntat fel.</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="255"/>
        <source>Add Symbolic Link</source>
        <translation>Lägg till symbolisk länk</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="258"/>
        <source>Add *.desktop file</source>
        <translation>Lägg till * .desktop-fil</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="300"/>
        <source>Remove AppImage</source>
        <translation>Ta bort AppImage</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="303"/>
        <source>Remove Icon</source>
        <translation>Ta bort ikon</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="306"/>
        <source>Remove Symbolic Link</source>
        <translation>Ta bort symbolisk länk</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="309"/>
        <source>Remove the *.desktop file</source>
        <translation>Ta bort * .desktop-filen</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="362"/>
        <source>View the *.desktop file</source>
        <translation>Visa *.desktop-filen</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="517"/>
        <source>Application for presenting, creating, or processing multimedia (audio/video).</source>
        <translation>Program för att presentera, skapa eller bearbeta multimedia (ljud / video).</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="519"/>
        <source>An audio application.</source>
        <translation>En ljudapplikation.</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="519"/>
        <source>A video application.</source>
        <translation>En videoapplikation.</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="520"/>
        <source>An application for development.</source>
        <translation>En applikation för utveckling.</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="520"/>
        <source>Educational software.</source>
        <translation>Utbildningsprogramvara.</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="521"/>
        <source>A game.</source>
        <translation>Ett spel.</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="522"/>
        <source>Application for viewing, creating, or processing graphics.</source>
        <translation>Applikation för visning, skapande eller bearbetning av grafik.</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="523"/>
        <source>Network application such as a web browser.</source>
        <translation>Nätverksapplikation som till exempel en webbläsare.</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="524"/>
        <source>An office type application.</source>
        <translation>Ett kontorsprogram.</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="524"/>
        <source>Scientific software.</source>
        <translation>Vetenskaplig programvara.</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="525"/>
        <source>Settings applications.</source>
        <translation>Program för att göra inställningar.</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="526"/>
        <source>System application, &quot;System Tools&quot; such as say a log viewer or network monitor.</source>
        <translation>Systemapplikation, &quot;Systemverktyg&quot; som t.ex. en loggvisare eller nätverksmonitor.</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="528"/>
        <source>Small utility application, &quot;Accessories&quot;.</source>
        <translation>Litet verktygsprogram, &quot;Tillbehör&quot;.</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="615"/>
        <source>must be restarted for the new language settings to take effect.</source>
        <translation>måste startas om för att de nya språkinställningarna ska börja gälla.</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="831"/>
        <source>Icon</source>
        <translation>Ikon</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="851"/>
        <source>Not executable</source>
        <translation>Ej körbar</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="864"/>
        <source>Missing</source>
        <translation>Saknas</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="867"/>
        <source>Missing!</source>
        <translation>Saknas!</translation>
    </message>
    <message>
        <location filename="../appimage.cpp" line="32"/>
        <source>Select AppImage</source>
        <translation>Välj AppImage</translation>
    </message>
    <message>
        <location filename="../appimage.cpp" line="33"/>
        <source>AppImage (*.AppImage)</source>
        <translation>AppImage (*.AppImage)</translation>
    </message>
    <message>
        <location filename="../appimage.cpp" line="42"/>
        <source>AppImage does not exist.</source>
        <translation>AppImage existerar inte.</translation>
    </message>
    <message>
        <location filename="../appimage.cpp" line="58"/>
        <location filename="../makedesktopfile.cpp" line="47"/>
        <location filename="../rewritedesktopfile.cpp" line="36"/>
        <location filename="../rewritedesktopfile.cpp" line="71"/>
        <location filename="../rewritedesktopfile.cpp" line="95"/>
        <source>Failure!
The shortcut could not be created.
Check your file permissions.</source>
        <translation>Misslyckande!
Det gick inte att skapa genvägen.
Kontrollera filbehörigheterna.</translation>
    </message>
    <message>
        <location filename="../addshortcut.cpp" line="42"/>
        <source>Select icon file (Not required</source>
        <translation>Välj ikonfil (Inte obligatoriskt</translation>
    </message>
    <message>
        <location filename="../addshortcut.cpp" line="43"/>
        <source>Icon (*.ico *.png *.jpg *.jepg *.svg)</source>
        <translation>Icon (*.ico *.png *.jpg *.jepg *.svg)</translation>
    </message>
    <message>
        <location filename="../addshortcut.cpp" line="123"/>
        <source>Select Category</source>
        <translation>Välj kategori</translation>
    </message>
    <message>
        <location filename="../icon.cpp" line="48"/>
        <source>Select icon file (Not required) png, xpm and svg are standard formats</source>
        <translation>Välj ikonfil (inte obligatoriskt) png, xpm och svg är standardformat</translation>
    </message>
    <message>
        <location filename="../icon.cpp" line="49"/>
        <source>Icon (*.png *.xpm *.svg *.ico *.jpg *.jepg)</source>
        <translation>Icon (*.png *.xpm *.svg *.ico *.jpg *.jepg)</translation>
    </message>
    <message>
        <location filename="../symlink.cpp" line="29"/>
        <location filename="../symlink.cpp" line="84"/>
        <source>Select folder for the symbolic link</source>
        <translation>Välj mapp för den symboliska länken</translation>
    </message>
    <message>
        <location filename="../symlink.cpp" line="41"/>
        <location filename="../symlink.cpp" line="96"/>
        <location filename="../symlink.cpp" line="106"/>
        <source>Failure!</source>
        <translation>Misslyckande!</translation>
    </message>
    <message>
        <location filename="../symlink.cpp" line="42"/>
        <location filename="../symlink.cpp" line="97"/>
        <location filename="../symlink.cpp" line="107"/>
        <source>You do not have the right to create a link in this folder.</source>
        <translation>Du har inte rätt att skapa en länk i den här mappen.</translation>
    </message>
    <message>
        <location filename="../symlink.cpp" line="60"/>
        <location filename="../symlink.cpp" line="68"/>
        <location filename="../widget.cpp" line="822"/>
        <source>An unexpected error occurred while creating the symbolic link.</source>
        <translation>Ett oväntat fel uppstod när den symboliska länken skapades.</translation>
    </message>
    <message>
        <location filename="../symlink.cpp" line="121"/>
        <location filename="../symlink.cpp" line="129"/>
        <source>An unexpected error occurred while creating the symbolic link</source>
        <translation>Ett oväntat fel uppstod när den symboliska länken skulle skapas</translation>
    </message>
    <message>
        <location filename="../tableheader.cpp" line="28"/>
        <source>Created</source>
        <translation>Skapad</translation>
    </message>
    <message>
        <location filename="../tableheader.cpp" line="29"/>
        <source>Path to AppImage</source>
        <translation>Sökväg till AppImage</translation>
    </message>
    <message>
        <location filename="../tableheader.cpp" line="30"/>
        <source>Path to Icon</source>
        <translation>Sökväg till ikon</translation>
    </message>
    <message>
        <location filename="../tableheader.cpp" line="31"/>
        <source>Symbolic link</source>
        <translation>Symbolisk länk</translation>
    </message>
    <message>
        <location filename="../tableheader.cpp" line="32"/>
        <location filename="../tableheader.cpp" line="39"/>
        <source>*.desktop file in:
</source>
        <translation>*.desktop fil i:
</translation>
    </message>
    <message>
        <location filename="../tableheader.cpp" line="33"/>
        <source>Symbolic link in:
</source>
        <translation>Symbolisk länk i:
</translation>
    </message>
    <message>
        <location filename="../editdesktop.cpp" line="76"/>
        <source>Failed to edit *.desktop file.</source>
        <translation>Det gick inte att redigera *.desktop-filen.</translation>
    </message>
    <message>
        <location filename="../editdesktop.cpp" line="149"/>
        <source>Failed to create *.desktop file.</source>
        <translation>Det gick inte att skapa *.desktop-filen.</translation>
    </message>
    <message>
        <location filename="../editdesktop.cpp" line="49"/>
        <location filename="../editdesktop.cpp" line="124"/>
        <source>Failed to create .desktop file.</source>
        <translation>Det gick inte att skapa .desktop-filen.</translation>
    </message>
    <message>
        <location filename="../config.cpp" line="84"/>
        <location filename="../widget.cpp" line="421"/>
        <source>Select &quot;Update&quot; to update</source>
        <translation>Välj &quot;Uppdatera&quot; för att uppdatera</translation>
    </message>
</context>
<context>
    <name>CheckUpdate</name>
    <message>
        <location filename="../checkupdate.cpp" line="35"/>
        <location filename="../checkupdate.cpp" line="51"/>
        <location filename="../checkupdate.cpp" line="75"/>
        <location filename="../checkupdate.cpp" line="93"/>
        <location filename="../checkupdate.cpp" line="102"/>
        <location filename="../checkupdate.cpp" line="111"/>
        <location filename="../checkupdate.cpp" line="143"/>
        <location filename="../checkupdate.cpp" line="178"/>
        <location filename="../checkupdate.cpp" line="251"/>
        <location filename="../checkupdate.cpp" line="260"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="53"/>
        <location filename="../checkupdate.cpp" line="74"/>
        <location filename="../checkupdate.cpp" line="145"/>
        <location filename="../checkupdate.cpp" line="259"/>
        <source>No Internet connection was found.
Please check your Internet settings and firewall.</source>
        <translation>Ingen internetanslutning hittades.
Kontrollera dina Internetinställningar och brandvägg.</translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="91"/>
        <source>Your version of </source>
        <translation>Din version av </translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="92"/>
        <source> is newer than the latest official version. </source>
        <translation> är nyare än den senaste officiella versionen. </translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="101"/>
        <source>You have the latest version of </source>
        <translation>Du har senaste versonen av </translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="110"/>
        <source>There was an error when the version was checked.</source>
        <translation>Det inträffade ett fel när versionen kontrollerades.</translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="177"/>
        <source>
There was an error when the version was checked.</source>
        <translation>
Det inträffade ett fel när versionen kontrollerades.</translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="222"/>
        <source>Updates:</source>
        <translation>Uppdateringar:</translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="245"/>
        <source>There is a new version of </source>
        <translation>Det finns en ny version av </translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="247"/>
        <source>Latest version: </source>
        <translation>Senaste versionen: </translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="29"/>
        <source>No error.</source>
        <translation>Inga fel.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="33"/>
        <source>The remote server closed the connection prematurely, before the entire reply was received and processed.</source>
        <translation>Fjärrservern stängde anslutningen i förtid, innan hela svaret togs emot och bearbetades.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="37"/>
        <source>The remote host name was not found (invalid hostname).</source>
        <translation>Fjärrvärdnamnet hittades inte (ogiltigt värdnamn).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="41"/>
        <source>The connection to the remote server timed out.</source>
        <translation>Anslutningen till fjärrservern tog för lång tid.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="45"/>
        <source>The operation was canceled via calls to abort() or close() before it was finished.</source>
        <translation>Operationen avbröts via anrop till abort() eller close() innan den avslutades.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="49"/>
        <source>The SSL/TLS handshake failed and the encrypted channel could not be established. The sslErrors() signal should have been emitted.</source>
        <translation>SSL/TLS-handskakningen misslyckades och den krypterade kanalen kunde inte upprättas. Signalen sslErrors() borde ha sänts ut.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="53"/>
        <source>The connection was broken due to disconnection from the network, however the system has initiated roaming to another access point. The request should be resubmitted and will be processed as soon as the connection is re-established.</source>
        <translation>Anslutningen bröts på grund av avbrott från nätverket, men systemet har initierat roaming till en annan åtkomstpunkt. Begäran bör skickas in på nytt och kommer att behandlas så snart anslutningen återupprättats.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="57"/>
        <source>The connection was broken due to disconnection from the network or failure to start the network.</source>
        <translation>Anslutningen bröts på grund av avbrott från nätverket eller misslyckande med att starta nätverket.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="61"/>
        <source>The background request is not currently allowed due to platform policy.</source>
        <translation>Bakgrundsbegäran är för närvarande inte tillåten på grund av plattformspolicy.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="65"/>
        <source>While following redirects, the maximum limit was reached. The limit is by default set to 50 or as set by QNetworkRequest::setMaxRedirectsAllowed().</source>
        <translation>Omdirigeringarna nådde maxgränsen. Gränsen är som standard inställd på 50 eller enligt QNetworkRequest::setMaxRedirectsAllowed().</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="69"/>
        <source>While following redirects, the network access API detected a redirect from a encrypted protocol (https) to an unencrypted one (http).</source>
        <translation>Vid omdirigeringar upptäckte API:et för nätverksåtkomst en omdirigering från ett krypterat protokoll (https) till ett okrypterat (http).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="73"/>
        <source>An unknown network-related error was detected.</source>
        <translation>Ett okänt nätverksrelaterat fel upptäcktes.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="77"/>
        <source>The connection to the proxy server was refused (the proxy server is not accepting requests).</source>
        <translation>Anslutningen till proxyservern nekades (proxyservern accepterar inte förfrågningar).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="81"/>
        <source>The proxy server closed the connection prematurely, before the entire reply was received and processed.</source>
        <translation>Proxyservern stängde anslutningen i förtid, innan hela svaret togs emot och bearbetades.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="85"/>
        <source>The proxy host name was not found (invalid proxy hostname).</source>
        <translation>Proxyvärdnamnet hittades inte (ogiltigt proxyvärdnamn).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="89"/>
        <source>The connection to the proxy timed out or the proxy did not reply in time to the request sent.</source>
        <translation>Anslutningen till proxyn tog för läng tid eller så svarade proxyn inte i tid på den skickade begäran.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="93"/>
        <source>The proxy requires authentication in order to honour the request but did not accept any credentials offered (if any).</source>
        <translation>Proxyn kräver autentisering för att uppfylla begäran men accepterade inte erbjudna användarnamn och lösenord (eller så har inga användarnamn och lösenord skickats).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="97"/>
        <source>An unknown proxy-related error was detected.</source>
        <translation>Ett okänt proxyrelaterat fel upptäcktes.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="101"/>
        <source>The access to the remote content was denied (similar to HTTP error 403).</source>
        <translation>Åtkomsten till fjärrinnehållet nekades (liknande HTTP-fel 403).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="105"/>
        <source>The operation requested on the remote content is not permitted.</source>
        <translation>Åtgärden som begärs på fjärrinnehållet är inte tillåten.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="109"/>
        <source>The remote content was not found at the server (similar to HTTP error 404).</source>
        <translation>Fjärrinnehållet hittades inte på servern (liknande HTTP-fel 404).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="113"/>
        <source>The remote server requires authentication to serve the content but the credentials provided were not accepted (if any).</source>
        <translation>Fjärrservern kräver autentisering för att visa innehållet men angivna användarnamn och lösenord accepterades inte (eller så har inget användarnamn och lösenord skickats).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="117"/>
        <source>The request needed to be sent again, but this failed for example because the upload data could not be read a second time.</source>
        <translation>Begäran behövde skickas igen, men detta misslyckades till exempel eftersom uppladdningsdata inte kunde läsas en andra gång.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="121"/>
        <source>The request could not be completed due to a conflict with the current state of the resource.</source>
        <translation>Begäran kunde inte slutföras på grund av en konflikt med resursens nuvarande tillstånd.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="125"/>
        <source>The requested resource is no longer available at the server.</source>
        <translation>Den begärda resursen är inte längre tillgänglig på servern.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="129"/>
        <source>An unknown error related to the remote content was detected.</source>
        <translation>Ett okänt fel relaterat till fjärrinnehållet upptäcktes.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="133"/>
        <source>The Network Access API cannot honor the request because the protocol is not known.</source>
        <translation>Network Access API kan inte uppfylla begäran eftersom protokollet inte är känt.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="137"/>
        <source>The requested operation is invalid for this protocol.</source>
        <translation>Den begärda åtgärden är ogiltig för detta protokoll.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="141"/>
        <source>A breakdown in protocol was detected (parsing error, invalid or unexpected responses, etc.).</source>
        <translation>Ett haveri i protokollet upptäcktes (analysfel, ogiltiga eller oväntade svar, etc.).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="145"/>
        <source>The server encountered an unexpected condition which prevented it from fulfilling the request.</source>
        <translation>Servern stötte på ett oväntat tillstånd som hindrade den från att uppfylla begäran.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="149"/>
        <source>The server does not support the functionality required to fulfill the request.</source>
        <translation>Servern stöder inte den funktionalitet som krävs för att uppfylla begäran.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="153"/>
        <source>The server is unable to handle the request at this time.</source>
        <translation>Servern kan inte hantera begäran just nu.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="157"/>
        <source>Unknown Server Error.</source>
        <translation>Okänt serverfel.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="161"/>
        <source>The remote server refused the connection (the server is not accepting requests).</source>
        <translation>Fjärrservern vägrade anslutningen (servern accepterar inte förfrågningar).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="165"/>
        <source>Unknown Error.</source>
        <translation>Okänt fel.</translation>
    </message>
</context>
<context>
    <name>Update</name>
    <message>
        <location filename="../update.cpp" line="34"/>
        <location filename="../update.cpp" line="67"/>
        <source>Update</source>
        <translation>Uppdatera</translation>
    </message>
    <message>
        <location filename="../update.cpp" line="35"/>
        <source>zsync cannot be found in path:
</source>
        <translation>zsync kan inte hittas i sökvägen:
</translation>
    </message>
    <message>
        <location filename="../update.cpp" line="36"/>
        <source>Unable to update.</source>
        <translation>Kan inte uppdatera.</translation>
    </message>
    <message>
        <location filename="../update.cpp" line="68"/>
        <source>An unexpected error occurred. Error message:</source>
        <translation>Ett oväntat fel uppstod. Felmeddelande:</translation>
    </message>
    <message>
        <location filename="../update.cpp" line="78"/>
        <source>is updated.</source>
        <translation>är uppdaterad.</translation>
    </message>
    <message>
        <location filename="../update.cpp" line="79"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
</context>
<context>
    <name>UpdateDialog</name>
    <message>
        <location filename="../updatedialog.ui" line="20"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <location filename="../updatedialog.cpp" line="31"/>
        <source>Update</source>
        <translation>Uppdatera</translation>
    </message>
    <message>
        <location filename="../updatedialog.cpp" line="47"/>
        <source>Updating, please wait...</source>
        <translation>Uppdaterar, vänta...</translation>
    </message>
</context>
</TS>
