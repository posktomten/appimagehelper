--------------------------------------------------
MD5 HASH SUM
4b38ec17efbd7c1a7c09363380c969cc  appimagehelper-i386.AppImage
Created: 2022-11-06T18-56-01
Size: 30.554733 MB
--------------------------------------------------
HOW TO CHECK THE HASH SUM
--------------------------------------------------
LINUX
"md5sum" is included in most Linus distributions.
md5sum appimagehelper-i386.AppImage
--------------------------------------------------
WINDOWS
"certutil" is included in Windows.
certutil -hashfile appimagehelper-i386.AppImage md5
--------------------------------------------------
READ MORE
https://gitlab.com/posktomten
--------------------------------------------------
