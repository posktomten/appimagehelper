//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          appImageHelper
//          Copyright (C) 2020 Ingemar Ceicer
//          http://ceicer.org/ingemar/
//          programmering1 (at) ceicer (dot) org
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3
//   as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#include "widget.h"
#include "ui_widget.h"

QString Widget::selectAppImage()
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       "appimagehelper");
    #if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
  settings.setIniCodec("UTF-8");
#endif
    settings.beginGroup("DirectoryPath");
    QString appimageDirectory = settings.value("appimageDirectory", QDir::homePath()).toString();
    settings.endGroup();
    QString appimage = QFileDialog::getOpenFileName(
                           nullptr, tr("Select AppImage"), appimageDirectory,
                           tr("AppImage (*.AppImage)"));

    if(appimage.isEmpty()) {
        return "NOTHING";
    }

    if(!checkIfExists(&appimage)) {
        QMessageBox::critical(
            nullptr, DISPLAY_NAME " " VERSION,
            tr("AppImage does not exist."));
        return "NOTHING";
    }

    QFileInfo fi(appimage);
    QString path = fi.absolutePath();
    settings.beginGroup("DirectoryPath");
    settings.setValue("appimageDirectory", path);
    settings.endGroup();
    QFile::setPermissions(appimage, QFileDevice::ReadOwner | QFileDevice::ExeOther | QFileDevice::ReadOther |
                          QFileDevice::ExeOwner | QFileDevice::WriteOwner | QFileDevice::ExeUser | QFileDevice::ReadUser);
    QFileInfo fi2(appimage);

    if(!fi2.isExecutable()) {
        QMessageBox::critical(
            nullptr, DISPLAY_NAME " " VERSION,
            tr("Failure!\nThe shortcut could not be created.\nCheck your file permissions."));
        return "NOTHING";
    }

    return appimage;
}
