//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          appImageHelper
//          Copyright (C) 2020 Ingemar Ceicer
//          http://ceicer.org/ingemar/
//          programmering1 (at) ceicer (dot) org
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3
//   as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#include "widget.h"
#include "ui_widget.h"

void Widget::removeShortcuts()
{
    QString tabort;
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       "appimagehelper");
    #if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
  settings.setIniCodec("UTF-8");
#endif
    settings.beginGroup("Path");
    QStringList keys = settings.allKeys();

    for(int row = 0; row < ui->tableWidget->rowCount() ; row++) {
        if(ui->tableWidget->item(row, 1)->isSelected()) {
            removePathToExecutable(row);
            return;
        }

        if(ui->tableWidget->item(row, 2)->isSelected()) {
            removePathToIkon(row);
            return;
        }

        QString nyckel = toSekunder(ui->tableWidget->item(row, 0)->text());
        QStringList list = settings.value(nyckel).toStringList();

        for(int i = 0; i < list.size(); i++) {
            if(ui->tableWidget->item(row, i + 1)->isSelected()) {
                tabort = list.at(i);
                QFileInfo fi(tabort);

                if(fi.exists()) {
                    if(QFile::remove(tabort)) {
                        list[i] = "NOTHING";
                    }
                } else {
                    list[i] = "NOTHING";
                }
            }
        }

        settings.setValue(nyckel, list);
    }

    settings.endGroup();
}

bool Widget::removePathToExecutable()
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       "appimagehelper");
    #if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
  settings.setIniCodec("UTF-8");
#endif
    settings.beginGroup("Path");
    QStringList keys = settings.allKeys();
    settings.endGroup();

    for(int row = 0; row < ui->tableWidget->rowCount(); row++) {
        if(ui->tableWidget->item(row, 1)->isSelected()) {
            QString tabort;
            QString nyckel = ui->tableWidget->item(row, 0)->text();
            nyckel = toSekunder(nyckel);
            QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                               "appimagehelper");
            #if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
  settings.setIniCodec("UTF-8");
#endif
            settings.beginGroup("Path");
            QStringList list = settings.value(nyckel).toStringList();

            for(int i = 2; i < list.size(); i++) {
                tabort = list.at(i);

                if(!tabort.isEmpty()) {
                    QFile::remove(tabort);
                }
            }

            QStringList keys = settings.allKeys();

            for(int i = 0; i < keys.length(); i++) {
                if(keys.at(i) == nyckel) {
                    settings.remove(keys.at(i));
                }
            }

            settings.endGroup();
            settings.beginGroup("Categories");
            QStringList keylist = settings.allKeys();

            foreach(QString s, keylist) {
                QStringList values = settings.value(s).toStringList();

                if(values.contains(nyckel)) {
                    values.removeAll(nyckel);
                }

                settings.setValue(s, values);
            }
        }
    }

    return true;
}
bool Widget::removePathToExecutable(int row)
{
    QString tabort;
    QString nyckel = ui->tableWidget->item(row, 0)->text();
    nyckel = toSekunder(nyckel);
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       "appimagehelper");
    #if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
  settings.setIniCodec("UTF-8");
#endif
    settings.beginGroup("Path");
    QStringList list = settings.value(nyckel).toStringList();

    for(int i = 3; i < list.size(); i++) {
        tabort = list.at(i);

        if(!tabort.isEmpty()) {
            QFile::remove(tabort);
        }
    }

    QStringList keys = settings.allKeys();

    for(int i = 0; i < keys.length(); i++) {
        if(keys.at(i) == nyckel) {
            settings.remove(keys.at(i));
        }
    }

    settings.endGroup();
    settings.beginGroup("Categories");
    QStringList keylist = settings.allKeys();

    foreach(QString s, keylist) {
        QStringList values = settings.value(s).toStringList();

        if(values.contains(nyckel)) {
            values.removeAll(nyckel);
        }

        settings.setValue(s, values);
    }

    return true;
}
bool Widget::removePathToIkon(int row)
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       "appimagehelper");
    #if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
  settings.setIniCodec("UTF-8");
#endif
    settings.beginGroup("Path");
    QString nyckel = toSekunder(ui->tableWidget->item(row, 0)->text());
    QStringList list = settings.value(nyckel).toStringList();
    settings.endGroup();

    for(int i = 3; i < list.size(); i++) {
        if(i != 4) {
            if(list.at(i) != "NOTHING") {
                rewriteDesktopFileNoIcon(list);
            }
        }
    }

    list[1] = "NOTHING";
    settings.beginGroup("Path");
    settings.setValue(nyckel, list);
    settings.endGroup();
    return true;
}
void Widget::cleanUp()
{
    QString path = "";
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       "appimagehelper");
    #if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
  settings.setIniCodec("UTF-8");
#endif
    QString nyckel;
    QStringList values;

    for(int row = 0; row < ui->tableWidget->rowCount(); row++) {
        nyckel = toSekunder(ui->tableWidget->item(row, 0)->text());
        settings.beginGroup("Path");
        values = settings.value(nyckel).toStringList();
        settings.endGroup();

        for(int i = 0; i < values.size(); i++) {
            if(ui->tableWidget->item(row, i + 1)->background() == Qt::red) {
                if(i == 0) {
                    removeRow(values);
                    settings.beginGroup("Path");
                    settings.remove(nyckel);
                    settings.endGroup();
                    return;
                } else {
                    path = values.at(i);

                    if(!QFile::exists(path)) {
                        values[i] = "NOTHING";
                        QFile::remove(QDir::toNativeSeparators(values.at(i)));
                    }

                    settings.beginGroup("Path");
                    settings.setValue(nyckel, values);
                    settings.endGroup();
                }
            }
        }
    }
}

void Widget::removeRow(QStringList values)
{
    int size = values.size();

    for(int i = 2; i < size; i++) {
        QFile::remove(QDir::toNativeSeparators(values.at(i)));
    }
}
