//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          AppImageLauncher
//          Copyright (C) 2020 Ingemar Ceicer
//          http://ceicer.org/ingemar/
//          programmering1 (at) ceicer (dot) org
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation version 3.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#ifndef WIDGET_H
#define WIDGET_H

#include <QDebug>
#include <QFileDialog>
#include <QFontDatabase>
#include <QMessageBox>
#include <QProcess>
#include <QStandardPaths>
#include <QTextStream>
#include <QWidget>
#include <QAction>
#include <QTableWidget>
#include <QSettings>
#include <QMenu>
#include <QDesktopServices>
#include <QScreen>
#include <QInputDialog>
#include <QSizePolicy>
#include <QDateTime>
#include <QClipboard>
#include <QTimer>
#include <QProcessEnvironment>
#include <QProgressDialog>
#include <QMenuBar>



#include "info.h"
#include "categories.h"
#include "dialog.h"
//included
#include "checkupdate.h"
#include "update.h"
#include "updatedialog.h"

QT_BEGIN_NAMESPACE
namespace Ui
{
class Widget;
}
QT_END_NAMESPACE

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();

public slots:
    void setEndConfig();

private:
    Ui::Widget *ui;
    bool fileExists(QString &path);
    QStringList items;
    QStringList itemsExplained;
    int index;
    void checkShortcuts();
    void removeShortcuts();
    bool removePathToExecutable();
    bool removePathToExecutable(int row);
    void cleanUp();
    bool checkIfExists(QString *source);
    void rightClick(QPoint pos);
    void setStartConfig();
    QVector<QCheckBox*> checkboxes;
    QStringList allApplicationsLocation;
    bool someoneTicked();
    QString selectIcon();
    QString selectAppImage();
    // Första gången
    bool makeDesktopFile(const QString &source, const QString &target, const QString &fileName, const QString &ikon, const QString &comment, const QString &categories);
    // Redigerad
    bool makeDesktopFile2(const QString &source, const QString &target, const QString &fileName, const QString &ikon, const QStringList &kategorier, const QString &sekunder);
    QString symLink(QString source);
    QString symLink(QString source, QString sekunder);
    QString indextid_iso;
    QString indextid_sec;
    void setRubriker();
    bool addShortcut(int row, int col);
    QString toSekunder(QString isoDateTime);
    QString toIsoDateTime(QString sekunder);
    bool targetDesktopLink(QString source, QString fileName, QString sekunder);
    int getCategory();
    bool removePathToIkon(int row);
    void visa();
    QString desktopFile();
    QString friendlyName(QString path);
    void removeRow(QStringList values);
    void rewriteDesktopFileNoIcon(QStringList &list);
    void rewriteDesktopFileIcon(QStringList &list, QString &ikon, int &i);

    UpdateDialog *ud;

public slots:
    void editDesktopFile(const QStringList &kategorier, const QString &nyckel);
    void editCreateDesktopFile(const QStringList &kategorier, const QString &nyckel, int &col);
    void foundUpdate(bool doUpdate);
    void isUpdated(bool isupdated);
    void getEnable();

};
#endif // WIDGET_H
