//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          appImageHelper
//          Copyright (C) 2020 Ingemar Ceicer
//          http://ceicer.org/ingemar/
//          programmering1 (at) ceicer (dot) org
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3
//   as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#include "widget.h"
#include "ui_widget.h"
bool Widget::addShortcut(int row, int col)
{
    if(col == 0 || col == 1) {
        return false;
    }

    QString nyckel = ui->tableWidget->item(row, 0)->text();
    nyckel = toSekunder(nyckel);
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       "appimagehelper");
    #if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
  settings.setIniCodec("UTF-8");
#endif
    settings.beginGroup("Path");
    QStringList list = settings.value(nyckel).toStringList();
    settings.endGroup();

    if(col == 2) { // Icon
        settings.beginGroup("DirectoryPath");
        QString iconDirektory = settings.value("iconDirectory", QDir::homePath()).toString();
        settings.endGroup();
        QString ikon = QFileDialog::getOpenFileName(
                           nullptr, tr("Select icon file (Not required"), iconDirektory,
                           tr("Icon (*.ico *.png *.jpg *.jepg *.svg)"));

        if(ikon.isEmpty()) {
            return false;
        }

        QFileInfo fi(ikon);
        QString path = fi.absolutePath();
        settings.beginGroup("DirectoryPath");
        settings.setValue("iconDirectory", path);
        settings.endGroup();
        list[1] = ikon;
        settings.beginGroup("Path");
        settings.setValue(nyckel, list);
        QStringList list2 = settings.value(nyckel).toStringList();
        settings.endGroup();

        for(int i = 3; i < list.size(); i++) {
            if(i != 4) {
                if(list.at(i) != "NOTHING") {
                    rewriteDesktopFileIcon(list, ikon, i);
                }
            }
        }

        return true;
    }

    QString appimage = list.at(0);
    QFileInfo fi(appimage);
    QString fileName = fi.fileName();

    if(col == 3) { // Symlink at any location
        QString tmp = symLink(appimage, nyckel);

        if(tmp == "NOTHING")  {
            return false;
        }

        list[2] = tmp;
        settings.beginGroup("Path");
        settings.setValue(nyckel, list);
        settings.endGroup();
        return true;
    }

    if(col == 5) { // Symlink on Desktop
        if(targetDesktopLink(appimage, fileName, nyckel)) {
            list[4] = QStandardPaths::writableLocation(QStandardPaths::DesktopLocation) + "/" + fileName + "_" + nyckel;
            settings.beginGroup("Path");
            settings.setValue(nyckel, list);
            settings.endGroup();
            return true;
        } else {
            return false;
        }
    }

    return true;
}
QString Widget::toSekunder(QString isoDateTime)
{
    QDateTime dt;
    dt = QDateTime::fromString(isoDateTime, Qt::ISODate);
    qint64 t = dt.toSecsSinceEpoch();
    QString sekunder = QString::number(t);
    return sekunder;
}
QString Widget::toIsoDateTime(QString sekunder)
{
    qint64 idt = static_cast<quint64>(sekunder.toULongLong());
    QDateTime dt = QDateTime::fromSecsSinceEpoch(idt);
    QString isoDateTime = dt.toString(Qt::ISODate);
    return isoDateTime;
}
int Widget::getCategory()
{
    QInputDialog dialog;
    dialog.setOptions(QInputDialog::UseListViewForComboBoxItems);
    dialog.setComboBoxItems(items);
    dialog.setWindowTitle(tr("Select Category"));
    dialog.exec();
    int index = items.indexOf(dialog.textValue());
    return index;
}
