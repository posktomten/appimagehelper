//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          appImageHelper
//          Copyright (C) 2020 Ingemar Ceicer
//          http://ceicer.org/ingemar/
//          programmering1 (at) ceicer (dot) org
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3
//   as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#include "widget.h"
#include "ui_widget.h"
QString Widget::selectIcon()
{
    bool isChecked = false;

    if((ui->chDesktop->isChecked() && ui->chStandardDesktopFile->isChecked())) {
        isChecked = true;
    }

    foreach(QCheckBox *cb, checkboxes) {
        if(cb->isChecked()) {
            isChecked = true;
            break;
        }
    }

    if(!isChecked) {
        return "NOTHING";
    }

    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       "appimagehelper");
    #if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
  settings.setIniCodec("UTF-8");
#endif
    settings.beginGroup("DirectoryPath");
    QString iconDirektory = settings.value("iconDirectory", QDir::homePath()).toString();
    settings.endGroup();
    QString ikon = QFileDialog::getOpenFileName(
                       nullptr, tr("Select icon file (Not required) png, xpm and svg are standard formats"), iconDirektory,
                       tr("Icon (*.png *.xpm *.svg *.ico *.jpg *.jepg)"));

    if(ikon.isEmpty()) {
        return "NOTHING";
    }

    QFileInfo fi(ikon);
    QString path = fi.absolutePath();
    settings.beginGroup("DirectoryPath");
    settings.setValue("iconDirectory", path);
    settings.endGroup();
    return ikon;
}
