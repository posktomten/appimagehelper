
#//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#//
#//          appImageHelper
#//          Copyright (C) 2020 Ingemar Ceicer
#//          http://ceicer.org/ingemar/
#//          programmering1 (at) ceicer (dot) org
#//
#//   This program is free software: you can redistribute it and/or modify
#//   it under the terms of the GNU General Public License version 3
#//   as published by the Free Software Foundation.
#//
#//   This program is distributed in the hope that it will be useful,
#//   but WITHOUT ANY WARRANTY; without even the implied warranty of
#//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#//   GNU General Public License for more details.
#//
#// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>


QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

#CONFIG += c++11
#CONFIG += /opt/Qt/5.15.1/gcc_64/bin/lrelease embed_translations

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

#CONFIG += static


SOURCES += \
    addshortcut.cpp \
    appimage.cpp \
    categories.cpp \
    check.cpp \
    config.cpp \
    dialog.cpp \
    editdesktop.cpp \
    friendlyname.cpp \
    icon.cpp \
    main.cpp \
    makedesktopfile.cpp \
    remove.cpp \
    rewritedesktopfile.cpp \
    symlink.cpp \
    tableheader.cpp \
    widget.cpp \
    info.cpp

HEADERS += \
    categories.h \
    dialog.h \
    widget.h \
    info.h

FORMS += \
    categories.ui \
    dialog.ui \
    widget.ui


    TRANSLATIONS += i18n/_appimagehelper_template_xx_XX.ts  \
                    i18n/_appimagehelper_sv_SE.ts

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target


unix:UI_DIR = ../code
win32:UI_DIR = ../code

RESOURCES += \
    resurser.qrc

    equals(QT_MAJOR_VERSION, 5) {
DESTDIR="../build-executable5"
unix: CONFIG  (release, debug|release): LIBS += -L../lib5/ -lcheckupdate # Release
else: unix: CONFIG (debug, debug|release): LIBS += -L../lib5/ -lchecupdatesd # Debug

unix: CONFIG  (release, debug|release): LIBS += -L../lib5/ -lupdateappimage # Release
else: unix: CONFIG (debug, debug|release): LIBS += -L../lib5/ -lupdateappimaged # Debug
INCLUDEPATH += ../include
}

equals(QT_MAJOR_VERSION, 6) {
DESTDIR="../build-executable6"
unix: CONFIG  (release, debug|release): LIBS += -L../lib6/ -lcheckupdate # Release
else: unix: CONFIG (debug, debug|release): LIBS += -L../lib6/ -lchecupdatesd # Debug

unix: CONFIG  (release, debug|release): LIBS += -L../lib5/ -lupdateappimage # Release
else: unix: CONFIG (debug, debug|release): LIBS += -L../lib6/ -lupdateappimaged # Debug
INCLUDEPATH += ../include
}
