//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          appImageHelper
//          Copyright (C) 2020 Ingemar Ceicer
//          http://ceicer.org/ingemar/
//          programmering1 (at) ceicer (dot) org
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3
//   as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#include "widget.h"
#include "ui_widget.h"
void Widget::editDesktopFile(const QStringList &kategorier, const QString &nyckel)
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       "appimagehelper");
    #if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
  settings.setIniCodec("UTF-8");
#endif
    QString ikon;
    settings.beginGroup("Path");
    QStringList list = settings.value(nyckel).toStringList();
    settings.endGroup();

    if(list.at(1) == "NOTHING") {
        ikon.clear();
    } else {
        ikon = list.at(1);
    }

    QString appimage = list.at(0);
    QFileInfo fi(appimage);
    QString fileName = fi.fileName();
    QString fil = QStandardPaths::writableLocation(QStandardPaths::DesktopLocation) + "/" + fileName + "_" + nyckel + ".desktop";

    if(QFile::exists(fil)) {
        const QString targetDesktopDESKTOP = QStandardPaths::writableLocation(QStandardPaths::DesktopLocation);

        if(!makeDesktopFile2(appimage, targetDesktopDESKTOP, fileName, ikon, kategorier, nyckel)) {
            QMessageBox::critical(
                nullptr, DISPLAY_NAME " " VERSION,
                tr("Failed to create .desktop file."));
        }

        list[3] = fil;
        settings.beginGroup("Path");
        settings.setValue(nyckel, list);
        settings.endGroup();
    }

    for(int raknare = 0; raknare < allApplicationsLocation.size(); raknare++) {
        int index = 5;
        int applikationLocationIndex = 0;
        QString s = allApplicationsLocation.at(applikationLocationIndex);
        QString filPath = s + "/" + fileName + "_" + nyckel + ".desktop";

        if(!QFile::exists(filPath)) {
            continue;
        }

        QFileInfo fi(s);

        if(fi.exists() && fi.isDir() && fi.isWritable()) {
            QFile f(filPath);

            if(!makeDesktopFile2(appimage, s, fileName, ikon, kategorier, nyckel)) {
                QMessageBox::critical(
                    nullptr, DISPLAY_NAME " " VERSION,
                    tr("Failed to edit *.desktop file."));
            }

            list[index] = allApplicationsLocation.at(applikationLocationIndex) + "/" + fileName + "_" + nyckel + ".desktop";
            settings.beginGroup("Path");
            settings.setValue(nyckel, list);
            settings.endGroup();
        }

        applikationLocationIndex++;
        index++;
    }

    checkShortcuts();
}


/*  */
void Widget::editCreateDesktopFile(const QStringList &kategorier, const QString &nyckel, int &col)
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       "appimagehelper");
    #if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
  settings.setIniCodec("UTF-8");
#endif
    QString ikon;
    settings.beginGroup("Path");
    QStringList list = settings.value(nyckel).toStringList();
    settings.endGroup();

    if(list.at(1) == "NOTHING") {
        ikon.clear();
    } else {
        ikon = list.at(1);
    }

    QString appimage = list.at(0);
    QFileInfo fi(appimage);
    QString fileName = fi.fileName();
    QString fil = QStandardPaths::writableLocation(QStandardPaths::DesktopLocation) + "/" + fileName + "_" + nyckel + ".desktop";

    if(col == 4) {
        if(!QFile::exists(fil)) {
            const QString targetDesktopDESKTOP = QStandardPaths::writableLocation(QStandardPaths::DesktopLocation);

            if(!makeDesktopFile2(appimage, targetDesktopDESKTOP, fileName, ikon, kategorier, nyckel)) {
                QMessageBox::critical(
                    nullptr, DISPLAY_NAME " " VERSION,
                    tr("Failed to create .desktop file."));
            }

            list[3] = fil;
            settings.beginGroup("Path");
            settings.setValue(nyckel, list);
            settings.endGroup();
        }
    } // if (col == 4)

    if(col > 5) {
        int index = col - 1;
        int applikationLocationIndex = col - 6;
        QString s = allApplicationsLocation.at(applikationLocationIndex);
        QString filPath = s + "/" + fileName + "_" + nyckel + ".desktop";

        if(!QFile::exists(filPath)) {
            QFileInfo fi(s);

            if(fi.exists() && fi.isDir() && fi.isWritable()) {
                QFile f(filPath);

                if(!makeDesktopFile2(appimage, s, fileName, ikon, kategorier, nyckel)) {
                    QMessageBox::critical(
                        nullptr, DISPLAY_NAME " " VERSION,
                        tr("Failed to create *.desktop file."));
                }

                list[index] = allApplicationsLocation.at(applikationLocationIndex) + "/" + fileName + "_" + nyckel + ".desktop";
                settings.beginGroup("Path");
                settings.setValue(nyckel, list);
                settings.endGroup();
            }
        }
    }  // col > 5

    editDesktopFile(kategorier, nyckel);
}
