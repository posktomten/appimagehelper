//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          appImageHelper
//          Copyright (C) 2020 Ingemar Ceicer
//          http://ceicer.org/ingemar/
//          programmering1 (at) ceicer (dot) org
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3
//   as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#include "categories.h"
#include "info.h"
#include "ui_categories.h"

Categories::Categories(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Categories)
{
    ui->setupUi(this);
    setAttribute(Qt::WA_DeleteOnClose);
    QIcon icon(":/images/appimagehelper.png");
    this->setWindowIcon(icon);
    this->setWindowTitle("Select Categories");
    QString fontPath = ":/fonts/Ubuntu-R.ttf";
    int fontId = QFontDatabase::addApplicationFont(fontPath);

    if(fontId != -1) {
        QFont f("Ubuntu", LINUX_FONT);
        QApplication::setFont(f);
    }

    itemsExplained
            << tr("Application for presenting, creating, or processing multimedia "
                  "(audio/video).")
            << tr("An audio application.") << tr("A video application.")
            << tr("An application for development.") << tr("Educational software.")
            << tr("A game.")
            << tr("Application for viewing, creating, or processing graphics.")
            << tr("Network application such as a web browser.")
            << tr("An office type application.") << tr("Scientific software.")
            << tr("Settings applications.")
            << tr("System application, \"System Tools\" such as say a log viewer or "
                  "network monitor.")
            << tr("Small utility application, \"Accessories\".");
    items << "AudioVideo"
          << "Audio"
          << "Video"
          << "Development"
          << "Education"
          << "Game"
          << "Graphics"
          << "Network"
          << "Office"
          << "Science"
          << "Settings"
          << "System"
          << "Utility";
    int i = 0;

    foreach(QString s, items) {
        QCheckBox *box = new QCheckBox;
        box->setText(s);
        box->setToolTip(itemsExplained.at(i++));
        ui->vl->addWidget(box);
        checkboxes.append(box);
    }

    connect(ui->pbSave, &QPushButton::clicked, [this]() {
        foreach(QCheckBox *ch, checkboxes) {
            if(ch->isChecked()) {
                triggerSignal();
                return;
            }
        }

        QMessageBox::critical(
            nullptr, DISPLAY_NAME,
            tr("You must select at least one category."));
        doValues();
    });
    connect(ui->pbExit, &QPushButton::clicked,  [this]() {
        close();
    });
}

void Categories::set(QString in, QPoint p, int row, int col, QString friendlyname, bool isNy)
{
    ny = isNy;
    friendlyName = friendlyname;
    invalue = in;
    column = col;
    this->move(p);
    ui->lb->setText(tr("These settings apply to all *.desktop files in line ") + QString::number(row + 1) + "\n" + tr("Categories for") + " " + friendlyName);
    doValues();
}
void Categories::doValues()
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       "appimagehelper");
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    settings.setIniCodec("UTF-8");
#endif
    settings.beginGroup("Categories");
    QStringList allKeys = settings.allKeys();

    foreach(QCheckBox *cb, checkboxes) {
        if(allKeys.contains(cb->text())) {
            QStringList list = settings.value(cb->text()).toStringList();

            if(list.contains(invalue)) {
                cb->setChecked(true);
            }
        }
    }

    settings.endGroup();
}
void Categories::triggerSignal()
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       "appimagehelper");
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    settings.setIniCodec("UTF-8");
#endif
    settings.beginGroup("Categories");
    QStringList allKeys = settings.allKeys();
    QStringList list;

    foreach(QString s, allKeys) {
        list = settings.value(s).toStringList();

        if(list.contains(invalue)) {
            list.removeAll(invalue);
        }

        settings.setValue(s, list);
    }

    settings.endGroup();
    settings.beginGroup("Categories");
    QStringList skicka;

    foreach(QCheckBox *cb, checkboxes) {
        if(cb->isChecked()) {
            QStringList list = settings.value(cb->text()).toStringList();
            list << invalue;
            settings.setValue(cb->text(), list);
            skicka << cb->text();
        }
    }

    settings.endGroup();

    if(ny) {
        emit sendCreate(skicka, invalue, column);
    } else {
        emit sendEdit(skicka, invalue);
    }
}


Categories::~Categories()
{
    emit sendEnable();
    delete ui;
}
