//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          appImageHelper
//          Copyright (C) 2020 Ingemar Ceicer
//          http://ceicer.org/ingemar/
//          programmering1 (at) ceicer (dot) org
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3
//   as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#include "widget.h"
#include "ui_widget.h"
void Widget::setStartConfig()
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       "appimagehelper");
    #if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
  settings.setIniCodec("UTF-8");
#endif
    settings.beginGroup("Geometry");
    QPoint punkt = settings.value("pos", QPoint(200, 200)).toPoint();
    this->move(settings.value("pos", QPoint(200, 200)).toPoint());
    QSize storlek = settings.value("size", QSize(700, 300)).toSize();
    this->resize(settings.value("size", QSize(700, 300)).toSize());
    int widd = 0, hojd = 0;
    ui->tableWidget->setSizeAdjustPolicy(QTableWidget::AdjustToContents);

    foreach(QScreen *screen, QGuiApplication::screens()) {
        widd = screen->size().width();
        hojd = screen->size().height();

        if((storlek.width() >= widd) || (storlek.height() >= hojd)) {
            this->resize(widd, hojd);
            this->setWindowState(Qt::WindowMaximized);
        }

        if((punkt.x() > widd - 100) || (punkt.y() > hojd - 100) || punkt.x() < 0 ||
                punkt.y() < 0) {
            this->move(200, 200);
        }
    }

    settings.endGroup();
    settings.beginGroup("Settings");
    ui->chChoice->setChecked(settings.value("chChoice").toBool());
    bool desktop = settings.value("chDesktop").toBool();

    if(desktop) {
        ui->chDesktop->setChecked(true);
        ui->chStandardDesktopFile->setEnabled(true);
        ui->chSoftLink->setEnabled(true);
    } else {
        ui->chDesktop->setChecked(false);
        ui->chStandardDesktopFile->setEnabled(false);
        ui->chSoftLink->setEnabled(false);
    }

    ui->chDesktop->setChecked(settings.value("chDesktop").toBool());
    ui->chStandardDesktopFile->setChecked(settings.value("chStandardDesktopFile").toBool());
    ui->chSoftLink->setChecked(settings.value("chSoftLink").toBool());
    ui->chDetails->setChecked(settings.value("chDetails").toBool());
    bool chnewversion = settings.value("chNewVersion").toBool();
    ui->chNewVersion->setChecked(chnewversion);
    settings.endGroup();

    if(!chnewversion) {
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                           "appimagehelper");
        #if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
  settings.setIniCodec("UTF-8");
#endif
        settings.beginGroup("Language");
        QString language = settings.value("language", "English").toString();
        settings.endGroup();
        CheckUpdate *cfu = new CheckUpdate;
        connect(cfu, &CheckUpdate::foundUpdate, this, &Widget::foundUpdate);
        QString *updateinstructions = new QString(tr("Select \"Update\" to update"));

        if(language == "Svenska") {
            cfu->checkOnStart(DISPLAY_NAME, VERSION, VERSION_PATH_SV, *updateinstructions);
        } else {
            cfu->checkOnStart(DISPLAY_NAME, VERSION, VERSION_PATH, *updateinstructions);
        }

        cfu = nullptr;
        delete cfu;
    }

    settings.beginGroup("Language");
    const QString language = settings.value("language", "English").toString();
    settings.endGroup();
    ui->combLanguage->setEditable(true);
    ui->combLanguage->lineEdit()->setReadOnly(true);
    ui->combLanguage->lineEdit()->setAlignment(Qt::AlignCenter);
    QIcon iconSwedish(":images/swedish.png");
    ui->combLanguage->addItem(iconSwedish, "Svenska");
    QIcon iconEnglish(":images/english.png");
    ui->combLanguage->addItem(iconEnglish, "English");

    if(language == "Svenska") {
        ui->combLanguage->setCurrentIndex(0);
    } else {
        ui->combLanguage->setCurrentIndex(1);
    }

    ui->tableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);
//    checkShortcuts();
}
void Widget::setEndConfig()
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       "appimagehelper");
    #if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
  settings.setIniCodec("UTF-8");
#endif
    settings.beginGroup("Geometry");
    settings.setValue("size", this->size());
    settings.setValue("pos", this->pos());
    settings.endGroup();
    settings.beginGroup("Settings");
    settings.setValue("chChoice", ui->chChoice->isChecked());
    settings.setValue("chDesktop", ui->chDesktop->isChecked());
    settings.setValue("chStandardDesktopFile", ui->chStandardDesktopFile->isChecked());
    settings.setValue("chSoftLink", ui->chSoftLink->isChecked());
    settings.setValue("chNewVersion", ui->chNewVersion->isChecked());
    settings.setValue("chDetails", ui->chDetails->isChecked());
    int index = 0;

    foreach(QCheckBox *chb, checkboxes) {
        settings.setValue("chb" + QString::number(index++), chb->isChecked());
    }

    settings.setValue("combSelect", ui->combSelect->currentIndex());
    settings.endGroup();
    settings.beginGroup("Language");
    settings.setValue("language", ui->combLanguage->currentText());
    settings.endGroup();
}
