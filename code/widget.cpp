//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          appImageHelper
//          Copyright (C) 2020 Ingemar Ceicer
//          http://ceicer.org/ingemar/
//          programmering1 (at) ceicer (dot) org
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3
//   as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent) : QWidget(parent), ui(new Ui::Widget)
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       "appimagehelper");
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    settings.setIniCodec("UTF-8");
#endif
    ui->setupUi(this);
    QIcon icon(":/images/appimagehelper.png");
    this->setWindowIcon(icon);
    this->setWindowTitle(DISPLAY_NAME " " VERSION);
    setStartConfig();
    const QString chortcutText = tr("*.desktop file in: ");
    const QString pathToDesktop = QStandardPaths::writableLocation(QStandardPaths::DesktopLocation);
    ui->chDesktop->setText(chortcutText + pathToDesktop);
    allApplicationsLocation =  QStandardPaths::standardLocations(QStandardPaths::ApplicationsLocation);
    int boxIndex = 0;
    settings.beginGroup("Settings");

    foreach(QString s, allApplicationsLocation) {
        QFileInfo fi(s);

        if(fi.exists() && fi.isDir() && fi.isWritable()) {
            QRegularExpression *re = new QRegularExpression("/home/.{1,}/.local/share/applications");
            QRegularExpressionMatch *match = new QRegularExpressionMatch(re->match(s));
            //QRegularExpression re2(QDir::homePath());
            // QRegularExpressionMatch *match2 = new QRegularExpressionMatch(re2.match(s));
            QCheckBox *box = new QCheckBox;
            ui->VerticalLayout_Shortcut->addWidget(box);
            box->setChecked(settings.value("chb" + QString::number(boxIndex++)).toBool());
            checkboxes.append(box);
            // Qt6
#if QT_VERSION >= QT_VERSION_CHECK(6, 0, 0)

            if(match->hasMatch()) {
                box->setText(chortcutText + s + tr("\n(Recommended)."));
            } else {
                box->setText(chortcutText + s);
            }

#endif
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)

            if(match->hasMatch()) {
                box->setText(&chortcutText + s + tr("\n(Recommended)."));
            } else {
                box->setText(&chortcutText + s);
            }

#endif
        }
    }

    settings.endGroup();
    // context menu
    this->setContextMenuPolicy(Qt::CustomContextMenu);
    QObject::connect(this, &QWidget::customContextMenuRequested, [this]() {
        QAction *deleteAll = new QAction(tr("Delete all settings"));
        QAction *forceUpdate = new QAction(tr("Force update"));
//        QAction *changelog = new QAction(tr("Read changelog"));
//        QAction *license = new QAction(tr("Read license"));
        QMenu *aplicationMenu = new QMenu;
        aplicationMenu->addAction(deleteAll);
        aplicationMenu->addAction(forceUpdate);
//        aplicationMenu->addAction(changelog);
//        aplicationMenu->addAction(license);
        QObject::connect(deleteAll, &QAction::triggered, this, [this]() {
            QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                               "appimagehelper");
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
            settings.setIniCodec("UTF-8");
#endif
            QString filnamn = settings.fileName();
            QFile fil(filnamn);

            if(!fil.exists())  {
                QMessageBox::critical(this, DISPLAY_NAME " " VERSION,
                                      tr("There is no configuration file."));
                return;
            }

            QMessageBox *msgBox = new QMessageBox(this);
            msgBox->setWindowTitle(DISPLAY_NAME " " VERSION);
            msgBox->setText(tr("All configuration files will be moved to the Trash and the program will close."));
            msgBox->setInformativeText(tr("All shortcuts created by ") + DISPLAY_NAME + tr(" will remain.\nDo you want to continue?"));
            msgBox->setStandardButtons(QMessageBox::No | QMessageBox::Yes);
            msgBox->setDefaultButton(QMessageBox::No);
            msgBox->setStyleSheet("QLabel{width: 600px; min-width: 600px; max-width: 600px;}");
            int ret = msgBox->exec();

            if(ret == QMessageBox::Yes) {
                if(fil.exists()) {
                    QFileInfo fi(filnamn);
                    QString mapp =  fi.absolutePath();
                    QDir dir(mapp);

                    if(! fil.moveToTrash(mapp)) {
                        QMessageBox::critical(nullptr, tr("Failed to move the configuration file to the Trash."),
                                              tr("Unexpected error."));
                    } else {
                        exit(0);
                    }
                } else {
                    QMessageBox::critical(nullptr, tr("Failed to move the configuration file to the Trash."),
                                          tr("There is no configuration file."));
                }
            }
        });
        // Force Update
        QObject::connect(forceUpdate, &QAction::triggered, this, [this]() {
            ud = new UpdateDialog;
            //    ud->setTitle(DISPLAY_NAME); // Title
            ud->show();
            // QObject class
            Update *up = new Update;
            // Contact is established, the slot "isUpdated" listens for a signal from the update class "isUpdated"
            connect(up, &Update::isUpdated, this, &Widget::isUpdated);
            // Pointer is sent so that "ud" can be deleted
            up->doUpdate(ZSYNC_ARG_1, ZSYNC_ARG_2, DISPLAY_NAME, ud);
        });
//        // Read CHANGELOG
//        QObject::connect(changelog, &QAction::triggered, this, []() {
//            QDesktopServices::openUrl(QUrl("CHANGELOG", QUrl::TolerantMode));
//        });
//        // Read LICENSE
//        QObject::connect(license, &QAction::triggered, this, []() {
//            QDesktopServices::openUrl(QUrl("LICENSE", QUrl::TolerantMode));
//        });
        aplicationMenu->exec(QCursor::pos());
        aplicationMenu->clear();
        aplicationMenu->deleteLater();
    });
// Högerklicksmeny
    ui->tableWidget->setContextMenuPolicy(Qt::CustomContextMenu);
    QObject::connect(ui->tableWidget, &QTableWidget::customContextMenuRequested, [this]() {
        int row = ui->tableWidget->currentRow();
        int col = ui->tableWidget->currentColumn();

        if(ui->tableWidget->columnCount() <= 0 || ui->tableWidget->rowCount() <= 0) {
            return;
        }

        QMenu *menu = new QMenu;
        /* END Högerklicksmeny */
        // Högerklicka o kopiera sökväg
        QAction *copy = new QAction(tr("Copy Path"));

        if(!ui->tableWidget->item(row, col)->text().isEmpty() && col != 0) {
            menu->addAction(copy);
        }

        QObject::connect(copy, &QAction::triggered, this, [this, row]() {
            int col = ui->tableWidget->currentColumn();

            if(row < 0) {
                return;
            }

            QString nyckel = ui->tableWidget->item(row, 0)->text();
            nyckel = toSekunder(nyckel);
            QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                               "appimagehelper");
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
            settings.setIniCodec("UTF-8");
#endif
            settings.beginGroup("Path");
            QStringList list = settings.value(nyckel).toStringList();
            settings.endGroup();
            QString s = list.at(col - 1);
            QClipboard * clipboard = QApplication::clipboard();
            clipboard->setText(s);
        });
        // Make AppImage executable
        QAction *makeexecutabla = new QAction(tr("Make the AppImage Executable"));

        if(col == 1) {
            menu->addAction(makeexecutabla);
        }

        QObject::connect(makeexecutabla, &QAction::triggered, this, [this, row]() {
            QString nyckel = ui->tableWidget->item(row, 0)->text();
            nyckel = toSekunder(nyckel);
            QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                               "appimagehelper");
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
            settings.setIniCodec("UTF-8");
#endif
            settings.beginGroup("Path");
            QStringList list = settings.value(nyckel).toStringList();
            settings.endGroup();
            QFile::setPermissions(list.at(0), QFileDevice::ReadOwner | QFileDevice::ExeOther | QFileDevice::ReadOther |
                                  QFileDevice::ExeOwner | QFileDevice::WriteOwner | QFileDevice::ExeUser | QFileDevice::ReadUser);
            QFileInfo fi2(list.at(0));

            if(!fi2.isExecutable()) {
                QMessageBox::critical(
                    nullptr, DISPLAY_NAME " " VERSION,
                    tr("Could not make the AppImage executable."));
            }

            checkShortcuts();
        });
        // Starta AppImage
        QAction *execute = new QAction(tr("Launch AppImage"));

        if(col == 1) {
            menu->addAction(execute);
        }

        QObject::connect(execute, &QAction::triggered, this, [this, row]() {
            QString nyckel = ui->tableWidget->item(row, 0)->text();
            nyckel = toSekunder(nyckel);
            QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                               "appimagehelper");
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
            settings.setIniCodec("UTF-8");
#endif
            settings.beginGroup("Path");
            QStringList list = settings.value(nyckel).toStringList();
            settings.endGroup();
            QProcess p;
            QString kor = list.at(0);
            p.startDetached(kor, QStringList() << "");
        });
        // Högerklicka o lägg till genväg
        QAction *shortcut = new QAction;

        if(ui->tableWidget->item(row, col)->text().isEmpty()) {
            if((col != 0) && (col != 1)) {
                if(col == 2) {
                    shortcut->setText(tr("Add Icon"));
                    menu->addAction(shortcut);
                } else if(col == 3 || col == 5) {
                    shortcut->setText(tr("Add Symbolic Link"));
                    menu->addAction(shortcut);
                } else {
                    shortcut->setText(tr("Add *.desktop file"));
                    menu->addAction(shortcut);
                }
            }
        }

        QObject::connect(shortcut, &QAction::triggered, this, [this, row, col]() {
            if(!ui->tableWidget->item(row, col)->text().isEmpty()) {
                QMessageBox::information(nullptr, tr("Unable to add a shortcut"),
                                         tr("There is already a shortcut here. You must remove it before you can create a new one."));
                return;
            }

            if(col == 4 || col > 5) {
                // Klass
                Categories *c = new Categories(nullptr);
                QPoint p;
                p.rx() = this->x() + 200;
                p.ry() = this->y() + 100;
                QString nyckel = ui->tableWidget->item(row, 0)->text();
                nyckel = toSekunder(nyckel);

                if(ui->chDetails->isChecked()) {
                    c->set(nyckel, p, row, col, friendlyName(ui->tableWidget->item(row, 1)->text()), true);
                } else {
                    c->set(nyckel, p, row, col, ui->tableWidget->item(row, 1)->text(), true);
                }

                QObject::connect(c, &Categories::sendCreate, this, &Widget::editCreateDesktopFile);
                QObject::connect(c, &Categories::sendEnable, this, &Widget::getEnable);
                c->show();
                this->setEnabled(false);
            }

            addShortcut(row, col);
            checkShortcuts();
        });
        // Högerklicka o ta bort genväg
        QAction *removeShortcut = new QAction;

        if((!ui->tableWidget->item(row, col)->text().isEmpty()) && (col != 0)) {
            if(col == 1) {
                removeShortcut->setText(tr("Remove AppImage"));
                menu->addAction(removeShortcut);
            } else if(col == 2) {
                removeShortcut->setText(tr("Remove Icon"));
                menu->addAction(removeShortcut);
            } else if(col == 3 || col == 5) {
                removeShortcut->setText(tr("Remove Symbolic Link"));
                menu->addAction(removeShortcut);
            } else {
                removeShortcut->setText(tr("Remove the *.desktop file"));
                menu->addAction(removeShortcut);
            }

            menu->addAction(removeShortcut);
        }

        QObject::connect(removeShortcut, &QAction::triggered, this, [this, row, col]() {
            if(ui->tableWidget->item(row, col)->text().isEmpty()) {
                return;
            }

            removePathToExecutable();
            removeShortcuts();
            checkShortcuts();
//            removeShortcuts();
//            checkShortcuts();
        });
        // Högerklicka o redigera Categories
        QAction *editCategories = new QAction(tr("Edit Categories"));

        if(col == 4 || col > 5) {
            if(!ui->tableWidget->item(row, col)->text().isEmpty()) {
                menu->addAction(editCategories);
            }
        }

        QObject::connect(editCategories, &QAction::triggered, this, [ this, row, col]() {
            if(ui->tableWidget->item(row, col)->text().isEmpty()) {
                return;
            }

            // Klass
            Categories *c = new Categories(nullptr);
            QPoint p;
            p.rx() = this->x() + 200;
            p.ry() = this->y() + 100;
            QString nyckel = ui->tableWidget->item(row, 0)->text();
            nyckel = toSekunder(nyckel);

            if(ui->chDetails->isChecked()) {
                c->set(nyckel, p, row, col, friendlyName(ui->tableWidget->item(row, 1)->text()), false);
            } else {
                c->set(nyckel, p, row, col, ui->tableWidget->item(row, 1)->text(), false);
            }

            QObject::connect(c, &Categories::sendEdit, this, &Widget::editDesktopFile);
            QObject::connect(c, &Categories::sendEnable, this, &Widget::getEnable);
            c->show();
            this->setEnabled(false);
            checkShortcuts();
        });
        // Högerklicka o kolla desktop filen
        QAction *editDesktop = new QAction(tr("View the *.desktop file"));

        if(col == 4 || col > 5) {
            if(!ui->tableWidget->item(row, col)->text().isEmpty()) {
                menu->addAction(editDesktop);
            }
        }

        QObject::connect(editDesktop, &QAction::triggered, this, [ this, row, col ]() {
            if(ui->tableWidget->item(row, col)->text().isEmpty()) {
                return;
            }

            QString nyckel = ui->tableWidget->item(row, 0)->text();
            nyckel = toSekunder(nyckel);
            QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                               "appimagehelper");
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
            settings.setIniCodec("UTF-8");
#endif
            settings.beginGroup("Path");
            QStringList list = settings.value(nyckel).toStringList();
            settings.endGroup();
            QString s = list.at(col - 1);
            // open .desktop file ipac
//            QDesktopServices::openUrl(QUrl("file:///" + s, QUrl::TolerantMode));
            QPoint p;
            p.rx() = this->x() + 200;
            p.ry() = this->y() + 100;
            Dialog *d = new Dialog(nullptr);
            d->setDialog(p, s);
            QObject::connect(d, &Dialog::sendEnable, this, &Widget::getEnable);
            d->show();
            this->setEnabled(false);
//            checkShortcuts();
        });
        menu->exec(QCursor::pos());
        menu->clear();
        removeShortcut->deleteLater();
        menu->deleteLater();
    });
    /* */
    /* */
// slut högerklick
//    Update
    connect(ui->pbMaintenanceTool, &QPushButton::clicked, [this]() -> void {

        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                           "appimagehelper");
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
        settings.setIniCodec("UTF-8");
#endif
        settings.beginGroup("Language");
        QString language = settings.value("language", "English").toString();
        settings.endGroup();

        CheckUpdate *cfu = new CheckUpdate;
        connect(cfu, &CheckUpdate::foundUpdate, this, &Widget::foundUpdate);

        QString *updateinstructions = new QString(tr("Select \"Update\" to update"));

        if(language == "Svenska")
        {
            cfu->check(DISPLAY_NAME, VERSION, VERSION_PATH_SV, *updateinstructions);
        } else
        {
            cfu->check(DISPLAY_NAME, VERSION, VERSION_PATH, *updateinstructions);
        }




        cfu = nullptr;
        delete cfu;

    });
    /*      */
//    Upgrade
    connect(ui->pbDoUpdate, &QPushButton::clicked, [this]() -> void {


        ud = new UpdateDialog;
        //    ud->setTitle(DISPLAY_NAME); // Title
        ud->show();
        // QObject class
        Update *up = new Update;
        // Contact is established, the slot "isUpdated" listens for a signal from the update class "isUpdated"
        connect(up, &Update::isUpdated, this, &Widget::isUpdated);
        // Pointer is sent so that "ud" can be deleted
        up->doUpdate(ZSYNC_ARG_1, ZSYNC_ARG_2, DISPLAY_NAME, ud);

    });
    /*      */
    /* Starta AppImage */
    connect(ui->pbExecite, &QPushButton::clicked, [this]() {
        int row = ui->tableWidget->currentRow();
        int col = ui->tableWidget->currentColumn();

        if(row < 0 || col < 0) {
            return;
        }

        QString nyckel = ui->tableWidget->item(row, 0)->text();
        nyckel = toSekunder(nyckel);
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                           "appimagehelper");
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
        settings.setIniCodec("UTF-8");
#endif
        settings.beginGroup("Path");
        QStringList list = settings.value(nyckel).toStringList();
        settings.endGroup();
        QProcess p;
        QString kor = list.at(0);
        p.startDetached(kor, QStringList() << "");
    });
    /* About */
    connect(ui->pbAbout, &QPushButton::clicked, []() {
        auto *information = new Info;
        information->getSystem();
        delete information;
    });
    /* Check shortcuts */
    connect(ui->pbRemoveShortcuts, &QPushButton::clicked, [this]() {
        checkShortcuts();
    });
    connect(ui->pbRemoveSelected, &QPushButton::clicked, [this]() {
        removePathToExecutable();
        removeShortcuts();
        checkShortcuts();
    });
    connect(ui->pbCleanUp, &QPushButton::clicked, [this]() {
        cleanUp();
        checkShortcuts();
    });
// Adjust size
    connect(ui->pbResizeWidth, &QPushButton::clicked, [this]() {
        int width = ui->tableWidget->width();

        if(ui->tableWidget->columnCount() <= 0) {
            return;
        }

        int delad_width = width / ui->tableWidget->columnCount();
        delad_width -= 4;

        for(int i = 0; i < ui->tableWidget->columnCount(); i++) {
            ui->tableWidget->setColumnWidth(i, delad_width);
        }
    });
// Resize to Content
    connect(ui->pbResizeContent, &QPushButton::clicked, [this]() {
        ui->tableWidget->resizeColumnsToContents();
    });
    itemsExplained
            << tr("Application for presenting, creating, or processing multimedia "
                  "(audio/video).")
            << tr("An audio application.") << tr("A video application.")
            << tr("An application for development.") << tr("Educational software.")
            << tr("A game.")
            << tr("Application for viewing, creating, or processing graphics.")
            << tr("Network application such as a web browser.")
            << tr("An office type application.") << tr("Scientific software.")
            << tr("Settings applications.")
            << tr("System application, \"System Tools\" such as say a log viewer or "
                  "network monitor.")
            << tr("Small utility application, \"Accessories\".");
    items << "AudioVideo"
          << "Audio"
          << "Video"
          << "Development"
          << "Education"
          << "Game"
          << "Graphics"
          << "Network"
          << "Office"
          << "Science"
          << "Settings"
          << "System"
          << "Utility";
// Klicka och visa innehållet i itemet
    connect(ui->tableWidget, &QTableWidget::cellClicked, [this](int row, int col) {
        if(ui->tableWidget->item(row, col)->text().isEmpty()) {
            return;
        }

        if(ui->chDetails->isChecked()) {
            return;
        }

        if(col == 0) {
            return;
        }

        QMenu *menu = new QMenu;
        QString nyckel = ui->tableWidget->item(row, 0)->text();
        nyckel = toSekunder(nyckel);
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                           "appimagehelper");
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
        settings.setIniCodec("UTF-8");
#endif
        settings.beginGroup("Path");
        QStringList list = settings.value(nyckel).toStringList();
        settings.endGroup();
        QAction *visa = new QAction();
        visa->setText(list.at(col - 1));
        menu->addAction(visa);
        QObject::connect(visa, &QAction::triggered, this, [menu, visa]() {
            menu->addAction(visa);
        });
        menu->exec(QCursor::pos());
        menu->clear();
        menu->deleteLater();
    });
    connect(ui->pbQuestion, &QPushButton::clicked, []() {
        QDesktopServices::openUrl(
            QUrl(MANUAL_PATH, QUrl::TolerantMode));
    });
// Items in "Typ av applikation"
    ui->combSelect->addItems(items);
    settings.beginGroup("Settings");
    ui->combSelect->setCurrentIndex(settings.value("combSelect").toInt());
    settings.endGroup();
//
//    View Details
//    connect(ui->chDetails, &QCheckBox::stateChanged, [ = ](int state) {
    connect(ui->chDetails, &QCheckBox::stateChanged, [ = ]() {
        checkShortcuts();
    });
// Avsluta
    connect(ui->pbExit, &QPushButton::clicked, this, QCoreApplication::quit);
//
    index = 0;
    connect(ui->combSelect, QOverload<int>::of(&QComboBox::activated),
    [ = ](int i) {
        index = i;
        ui->lblVisaVal->setText(itemsExplained.at(i));
    });
// language
    connect(ui->combLanguage, &QComboBox::currentTextChanged, [this](QString text) {
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                           "appimagehelper");
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
        settings.setIniCodec("UTF-8");
#endif
        settings.beginGroup("Language");
        QString language = settings.value("language", "English").toString();
        settings.endGroup();

        if(text != language) {
            QMessageBox msgBox;
            msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
            msgBox.setText(DISPLAY_NAME " " + tr("must be restarted for the new language settings to take effect."));
            msgBox.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
            msgBox.setDefaultButton(QMessageBox::Cancel);
            int ret = msgBox.exec();

            switch(ret) {
                case QMessageBox::Cancel:
                    if(language == "Svenska") {
                        ui->combLanguage->setCurrentIndex(0);
                    } else {
                        ui->combLanguage->setCurrentIndex(1);
                    }

                    break;

                case QMessageBox::Ok:
                    close();
                    settings.beginGroup("Language");
                    settings.setValue("language", text);
                    settings.endGroup();
                    settings.sync();
                    const QString EXECUTE = QDir::toNativeSeparators(
                                                QCoreApplication::applicationDirPath() + "/" +
                                                QFileInfo(QCoreApplication::applicationFilePath()).fileName());
                    QProcess p;
                    p.setProgram(EXECUTE);
                    p.startDetached();
            }
        }
    });
// ENEBLE/DISABLE
    connect(ui->chDesktop, &QCheckBox::stateChanged, [this](int state) {
        if(state == 2) {
            ui->chStandardDesktopFile->setEnabled(true);
            ui->chSoftLink->setEnabled(true);
        } else {
            ui->chStandardDesktopFile->setEnabled(false);
            ui->chSoftLink->setEnabled(false);
        }
    });
    /* SKAPA NY POST */
    connect(ui->pbDesktop, &QPushButton::clicked, [this]() {
        QDateTime dt(QDateTime::currentDateTime());
        qint64 tid = dt.toSecsSinceEpoch();
        QString tmp_tid = dt.toString(Qt::ISODate);
        QString stid = QString::number(tid);
        indextid_sec = stid;
        indextid_iso = tmp_tid;
        QString targetDesktopDESKTOP = "NOTHING";
        QString targetDesktopLINK = "NOTHING";
        QString target = "NOTHING";
        QString symlink = "NOTHING";
        QStringList targetDesktop;
        targetDesktop.clear();
        QString ikon = selectIcon();
        /* SLUT SELECT ICON */
        /* SELECT AppImage */
        QString source = selectAppImage();

        if(source == "NOTHING") {
            return;
        }

        /* SLUT SELECT AppImage */
        /**********/
        /* SYMLINK */
        if(ui->chChoice->isChecked()) {
            symlink = symLink(source);
        }

        /* SLUT SYMLINK */
        /* Kattegorier och kommentarer */
        QString categories = "";
        index = ui->combSelect->currentIndex();

        if(index == 1)
            categories = items.at(1) + ";" + items.at(0) + ";";
        else if(index == 2)
            categories = items.at(2) + ";" + items.at(0) + ";";
        else
            categories = items.at(index) + ";";

        QString comment = itemsExplained.at(index);
        /* SLUT Kattegorier och kommentarer */
        /**********/
        // Sökväg till AppImage
        QFileInfo info(source);
        // Namnet på AppImagen
        QString fileName = info.fileName();
        /* Alla dynamiskt skapade kryssrutorna */
        int location = 0;

        foreach(QCheckBox *s, checkboxes) {
            if(s->isChecked()) {
                target =
                    allApplicationsLocation.at(location) +
                    "/" + fileName;

                if(makeDesktopFile(source, target, fileName,  ikon, comment, categories)) {
                    targetDesktop << target;
                } else {
                    targetDesktop << "NOTHING";
                }
            } else {
                targetDesktop << "NOTHING";
            }

            location++;
        }

        /* SLUT Alla dynamiskt skapade kryssrutorna */
        // Desktop chortcut

        if(ui->chDesktop->isChecked()) {
            // STANDARD LINUS .desktop
            // ipac
            if(ui->chStandardDesktopFile->isChecked()) {
                targetDesktopDESKTOP =
                    QStandardPaths::writableLocation(QStandardPaths::DesktopLocation)
                    +
                    "/" + fileName;
                makeDesktopFile(source, targetDesktopDESKTOP, fileName,  ikon, comment, categories);
            } else {
                targetDesktopDESKTOP = "NOTHING";
            }

            // SLUT STANDARD LINUS .desktop
            // Linus soft link
            if(ui->chSoftLink->isChecked()) {
                targetDesktopLINK =
                    QStandardPaths::writableLocation(QStandardPaths::DesktopLocation) +
                    "/" + fileName;

                if(QFile::exists(targetDesktopLINK)) {
                    QFile::remove(targetDesktopLINK);
                }

                if(!QFile::link(source, targetDesktopLINK + "_" + indextid_sec)) {
                    targetDesktopLINK = "NOTHING";
                }
            }
        } else {
            targetDesktopDESKTOP = "NOTHING";
            targetDesktopLINK = "NOTHING";
        }

        // SLUT Linus soft link
        // SPARA I SETTINGS
        QStringList tmp_list;

        foreach(QString s, targetDesktop) {
            if(s != "NOTHING")
                s.append("_" + indextid_sec + ".desktop");

            tmp_list << s;
        }

        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                           "appimagehelper");
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
        settings.setIniCodec("UTF-8");
#endif
        settings.beginGroup("Path");
        QStringList allKeys = settings.allKeys();
        QStringList list;

        if(symlink != "NOTHING")
            symlink.append("_" + indextid_sec);

        if(targetDesktopDESKTOP != "NOTHING")
            targetDesktopDESKTOP.append("_" + indextid_sec + ".desktop");

        if(targetDesktopLINK != "NOTHING")
            targetDesktopLINK.append("_" + indextid_sec);

        list << source << ikon << symlink  << targetDesktopDESKTOP << targetDesktopLINK << tmp_list;
        settings.setValue(indextid_sec, list);
        settings.endGroup();
        settings.beginGroup("Categories");
        QStringList skicka;
        checkShortcuts();
    });
}

bool Widget::fileExists(QString & path)
{
    QFileInfo check_file(path);

    // check if file exists and if yes: Is it really a file and no directory?
    // and is it executable?
    if(check_file.exists() && check_file.isFile() && check_file.isExecutable()) {
        return true;
    } else {
        return false;
    }
}
bool Widget::targetDesktopLink(QString source, QString fileName, QString sekunder)
{
    QString targetDesktopLINK =
        QStandardPaths::writableLocation(QStandardPaths::DesktopLocation) +
        "/" + fileName;

    if(QFile::exists(targetDesktopLINK)) {
        QFile::remove(targetDesktopLINK);
    }

    if(!QFile::link(source, targetDesktopLINK + "_" + sekunder)) {
        QMessageBox::critical(nullptr, DISPLAY_NAME " " VERSION,
                              tr("An unexpected error occurred while creating the symbolic link."));
        return false;
    } else {
        return true;
    }
}
void Widget::visa()
{
    ui->tableWidget->horizontalHeaderItem(1)->setText("AppImage");
    ui->tableWidget->horizontalHeaderItem(2)->setText(tr("Icon"));

    for(int row = 0; row < ui->tableWidget->rowCount(); row++) {
        for(int col = 0; col < ui->tableWidget->columnCount(); col++) {
            QString text = ui->tableWidget->item(row, col)->text();

            if(ui->tableWidget->item(row, col)->background() == Qt::green) {
                if(col == 1) {
                    ui->tableWidget->item(row, col)->setText(friendlyName(text));
//                    ui->tableWidget->item(row, col)->setTextAlignment(Qt::AlignCenter);
                } else  if(col == 2) {
                    QIcon icon(text);
                    ui->tableWidget->item(row, col)->setText(" ");
                    ui->tableWidget->item(row, col)->setIcon(icon);
                } else {
                    ui->tableWidget->item(row, col)->setText("OK");
                    ui->tableWidget->item(row, col)->setTextAlignment(Qt::AlignCenter);
                }
            } else if(ui->tableWidget->item(row, col)->background() == Qt::yellow) {
                if(col == 1) {
                    ui->tableWidget->item(row, col)->setText(tr("Not executable") + " " + friendlyName(text));
//                    ui->tableWidget->item(row, col)->setTextAlignment(Qt::AlignCenter);
                } else  if(col == 2) {
                    QIcon icon(text);
                    ui->tableWidget->item(row, col)->setText(" ");
                    ui->tableWidget->item(row, col)->setIcon(icon);
                    ui->tableWidget->item(row, col)->setTextAlignment(Qt::AlignCenter);
                } else {
                    ui->tableWidget->item(row, col)->setText("OK");
                    ui->tableWidget->item(row, col)->setTextAlignment(Qt::AlignCenter);
                }
            } else if(ui->tableWidget->item(row, col)->background() == Qt::red) {
                if(col == 1) {
                    ui->tableWidget->item(row, col)->setText(tr("Missing") + " " + friendlyName(text));
                    ui->tableWidget->item(row, col)->setTextAlignment(Qt::AlignCenter);
                } else {
                    ui->tableWidget->item(row, col)->setText(tr("Missing!"));
                    ui->tableWidget->item(row, col)->setTextAlignment(Qt::AlignCenter);
                }
            }
        }
    }
}
void Widget::foundUpdate(bool doUpdate)
{
    if(doUpdate) {
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                           "appimagehelper");
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
        settings.setIniCodec("UTF-8");
#endif
        settings.beginGroup("Settings");
        settings.setValue("readyupdate", true);
        settings.endGroup();
        ui->pbDoUpdate->setEnabled(true);
    }
}

/*   */
/*  */

void Widget::isUpdated(bool isupdated)
{
    if(isupdated) {
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                           "appimagehelper");
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
        settings.setIniCodec("UTF-8");
#endif
        settings.beginGroup("Settings");
        settings.setValue("readyupdate", false);
        settings.endGroup();
        ui->pbDoUpdate->setEnabled(false);
        close();
    }
}
void Widget::getEnable()
{
    this->setEnabled(true);
}

Widget::~Widget()
{
    delete ui;
    this->setAttribute(Qt::WA_DeleteOnClose, true);
}
