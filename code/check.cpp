//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          appImageHelper
//          Copyright (C) 2020 Ingemar Ceicer
//          http://ceicer.org/ingemar/
//          programmering1 (at) ceicer (dot) org
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3
//   as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#include "widget.h"
#include "ui_widget.h"
void Widget::checkShortcuts()
{
    ui->tableWidget->clear();
    ui->tableWidget->setRowCount(0);
    ui->tableWidget->setColumnCount(0);
    setRubriker(); // tableheader.cpp
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       "appimagehelper");
    #if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
  settings.setIniCodec("UTF-8");
#endif
    settings.beginGroup("Path");
    QStringList keys = settings.allKeys();

    for(int row = 0; row < keys.size(); row++) {
        ui->tableWidget->insertRow(row);
//        ui->tableWidget->verticalHeader()->setDefaultSectionSize(30);
        QString test = QDir::toNativeSeparators(keys.at(row));
        QTableWidgetItem *item = new QTableWidgetItem(nullptr);
        item->setText(toIsoDateTime(test));
        item->setForeground(Qt::black);
        ui->tableWidget->setItem(row, 0, item);
    }

    for(int row = 0; row < keys.size(); row++) {
        QStringList values = settings.value(keys.at(row)).toStringList();
        int col = 1;

        foreach(QString v, values) {
            if(v == "NOTHING") {
                QTableWidgetItem *item = new QTableWidgetItem(nullptr);
                item->setText("");
                ui->tableWidget->setItem(row, col, item);
                col++;
            } else {
                QTableWidgetItem *item = new QTableWidgetItem(nullptr);
                QString test = QDir::toNativeSeparators("/" + v);
                QFileInfo *fileInfo = new QFileInfo(test);

                if(fileInfo->isExecutable() && fileInfo->exists()) {
                    item->setBackground(Qt::green);
                    item->setForeground(Qt::black);
                } else if(!fileInfo->exists()) {
                    item->setBackground(Qt::red);
                    item->setForeground(Qt::white);
                } else {
                    item->setBackground(Qt::yellow);
                    item->setForeground(Qt::black);
                }

//                if(col == 2) {
//                    QIcon icon(v);
//                    item->setIcon(icon);
//                    item->setText(v);
//                } else {
//                    item->setText(v);
//                }
                item->setText(v);
                ui->tableWidget->setItem(row, col, item);
                col++;
            }
        }
    }

    ui->tableWidget->sortByColumn(0, Qt::AscendingOrder);
    settings.endGroup();

    if(!ui->chDetails->isChecked()) {
        visa();
        ui->tableWidget->resizeColumnsToContents();
    } else {
        ui->tableWidget->resizeColumnsToContents();
    }
}



bool Widget::checkIfExists(QString *source)
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       "appimagehelper");
    #if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
  settings.setIniCodec("UTF-8");
#endif
    settings.beginGroup("Path");
    QStringList keys = settings.allKeys();
    QString tmp = *source;
    tmp.remove(0, 1);

    if(keys.contains(tmp)) {
        settings.remove(tmp);
        settings.endGroup();
        return true;
    }

    return true;
}


