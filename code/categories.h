//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          appImageHelper
//          Copyright (C) 2020 Ingemar Ceicer
//          http://ceicer.org/ingemar/
//          programmering1 (at) ceicer (dot) org
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3
//   as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#ifndef CATEGORIES_H
#define CATEGORIES_H

#include <QWidget>
#include <QCheckBox>
#include <QFontDatabase>
#include <QSettings>
#include <QDebug>
#include <QMessageBox>

#define DISPLAY_NAME "appImageHelper"
namespace Ui
{
class Categories;
}

class Categories : public QWidget
{
    Q_OBJECT

public:
    explicit Categories(QWidget *parent = nullptr);
    ~Categories();
    void set(QString in, QPoint p, int row, int col, QString friendlyname, bool isNy);

private:
    Ui::Categories *ui;
    QVector<QCheckBox*> checkboxes;
    QStringList items;
    QStringList itemsExplained;
    QString invalue;
    int column;
    void doValues();
    bool ny;
    QString friendlyName;

    void triggerSignal();
signals:
    void sendEdit(QStringList &kategorier, QString &nyckel);
    void sendCreate(QStringList &kategorier, QString &nyckel, int &col);
    void sendEnable();
};

#endif // CATEGORIES_H
