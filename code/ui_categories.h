/********************************************************************************
** Form generated from reading UI file 'categories.ui'
**
** Created by: Qt User Interface Compiler version 6.4.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CATEGORIES_H
#define UI_CATEGORIES_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Categories
{
public:
    QVBoxLayout *verticalLayout;
    QLabel *lb;
    QVBoxLayout *vl;
    QHBoxLayout *horizontalLayout;
    QPushButton *pbSave;
    QPushButton *pbExit;
    QSpacerItem *horizontalSpacer;

    void setupUi(QWidget *Categories)
    {
        if (Categories->objectName().isEmpty())
            Categories->setObjectName("Categories");
        Categories->resize(306, 381);
        verticalLayout = new QVBoxLayout(Categories);
        verticalLayout->setObjectName("verticalLayout");
        lb = new QLabel(Categories);
        lb->setObjectName("lb");

        verticalLayout->addWidget(lb);

        vl = new QVBoxLayout();
        vl->setObjectName("vl");

        verticalLayout->addLayout(vl);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName("horizontalLayout");
        pbSave = new QPushButton(Categories);
        pbSave->setObjectName("pbSave");

        horizontalLayout->addWidget(pbSave);

        pbExit = new QPushButton(Categories);
        pbExit->setObjectName("pbExit");

        horizontalLayout->addWidget(pbExit);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);


        verticalLayout->addLayout(horizontalLayout);


        retranslateUi(Categories);

        QMetaObject::connectSlotsByName(Categories);
    } // setupUi

    void retranslateUi(QWidget *Categories)
    {
        Categories->setWindowTitle(QCoreApplication::translate("Categories", "Form", nullptr));
        lb->setText(QCoreApplication::translate("Categories", "TextLabel", nullptr));
        pbSave->setText(QCoreApplication::translate("Categories", "Save", nullptr));
        pbExit->setText(QCoreApplication::translate("Categories", "Exit", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Categories: public Ui_Categories {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CATEGORIES_H
