//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          appImageHelper
//          Copyright (C) 2020 Ingemar Ceicer
//          http://ceicer.org/ingemar/
//          programmering1 (at) ceicer (dot) org
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3
//   as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#include "widget.h"
#include "ui_widget.h"

void Widget::rewriteDesktopFileNoIcon(QStringList &list)
{
    QStringList tmp;

    for(int i = 2; i < list.size(); i++) {
        tmp.clear();
        QString desktop = list.at(i);

        if(desktop.right(8) == ".desktop") {
            QFile file(list.at(i));

            if(file.open(QIODevice::ReadOnly | QIODevice::Text)) {
            } else {
                QMessageBox::critical(
                    nullptr, DISPLAY_NAME " " VERSION,
                    tr("Failure!\nThe shortcut could not be created.\nCheck your file permissions."));
                return;
            }

            QTextStream in(&file);

            while(!in.atEnd()) {
                QString line = in.readLine();
                tmp << line;
            }

            file.close();

            foreach(QString s, tmp) {
                if(s.left(6) == "Icon=/") {
                    tmp.removeAll(s);
                }
            }

            file.remove(list.at(i));

            if(file.open(QIODevice::WriteOnly | QIODevice::Text)) {
                QTextStream out(&file);

                foreach(QString s, tmp) {
                    out << s + '\n';
                }

                file.close();
                file.setPermissions(QFileDevice::ReadUser | QFileDevice::ExeUser |
                                    QFileDevice::WriteUser | QFileDevice::ReadOther |
                                    QFileDevice::ExeOther);
            } else {
                QMessageBox::critical(
                    nullptr, DISPLAY_NAME " " VERSION,
                    tr("Failure!\nThe shortcut could not be created.\nCheck your file permissions."));
                return;
            }
        }
    }
}

void Widget::rewriteDesktopFileIcon(QStringList &list, QString &ikon, int &i)
{
    QString desktop = list.at(i);

    if(desktop.right(8) == ".desktop") {
        QFile *file = new QFile(list.at(i));

        if(file->open(QIODevice::WriteOnly | QIODevice::Append | QIODevice::Text)) {
            QTextStream stream(file);
            stream << "Icon=" + ikon;
            file->close();
            file->setPermissions(QFileDevice::ReadUser | QFileDevice::ExeUser |
                                 QFileDevice::WriteUser | QFileDevice::ReadOther |
                                 QFileDevice::ExeOther);
        } else {
            QMessageBox::critical(
                nullptr, DISPLAY_NAME " " VERSION,
                tr("Failure!\nThe shortcut could not be created.\nCheck your file permissions."));
            return;
        }
    }
}
