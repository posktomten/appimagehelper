// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          appImageHelper
//          Copyright (C) 2020 Ingemar Ceicer
//          http://ceicer.org/ingemar/
//          programmering1 (at) ceicer (dot) org
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3
//   as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#include "widget.h"
#include "ui_widget.h"
QString Widget::symLink(QString source)
{
    QString symlink(QDir::homePath());
    bool fortsatt = true;
    QFile fil(source);
    QFileInfo fifil(fil);
    QString filnamn = fifil.fileName();

    do  {
        symlink = QFileDialog::getExistingDirectory(this, tr("Select folder for the symbolic link"),
                  symlink,
                  QFileDialog::ShowDirsOnly
                  | QFileDialog::DontResolveSymlinks);

        if(symlink.isEmpty()) {
            return "NOTHING";
        }

        QFileInfo fi(symlink);

        if(!(fi.isDir() && fi.isWritable())) {
            QMessageBox::warning(this, tr("Failure!"),
                                 tr("You do not have the right to create a link in this folder."));
        } else {
            fortsatt = false;
        }
    } while(fortsatt);

    QString fullSymlink = symlink + "/" + filnamn;
    QFile f(fullSymlink);

    if(f.exists()) {
        f.remove();
    }

    QString tmp = fullSymlink;
    tmp.append("_" + indextid_sec);

    if(!QFile::link(source, tmp)) {
        QMessageBox::critical(nullptr, DISPLAY_NAME " " VERSION,
                              tr("An unexpected error occurred while creating the symbolic link."));
        return "NOTHING";
    }

    QFileInfo fi2(tmp);

    if(!fi2.isSymLink()) {
        QMessageBox::critical(nullptr, DISPLAY_NAME " " VERSION,
                              tr("An unexpected error occurred while creating the symbolic link."));
        return "NOTHING";
    }

    return fullSymlink;
}

QString Widget::symLink(QString source, QString sekunder)
{
    bool fortsatt = true;
    QString symlink(QDir::homePath());
    QFile fil(source);
    QFileInfo fifil(fil);
    QString filnamn = fifil.fileName();

    do  {
        symlink = QFileDialog::getExistingDirectory(this, tr("Select folder for the symbolic link"),
                  symlink,
                  QFileDialog::ShowDirsOnly
                  | QFileDialog::DontResolveSymlinks);

        if(symlink.isEmpty()) {
            return "NOTHING";
        }

        QFileInfo fi(symlink);

        if(!(fi.isDir() && fi.isWritable())) {
            QMessageBox::warning(this, tr("Failure!"),
                                 tr("You do not have the right to create a link in this folder."));
        } else {
            fortsatt = false;
        }
    } while(fortsatt);

    QFileInfo fi(symlink);

    if(!(fi.isDir() && fi.isWritable())) {
        QMessageBox::warning(this, tr("Failure!"),
                             tr("You do not have the right to create a link in this folder."));
    }

    QString fullSymlink = symlink + "/" + filnamn;
    QString tmp = fullSymlink;
    tmp.append("_" + sekunder);
    QFile f(tmp);

    if(f.exists()) {
        f.remove();
    }

    if(!QFile::link(source, tmp)) {
        QMessageBox::critical(nullptr, DISPLAY_NAME " " VERSION,
                              tr("An unexpected error occurred while creating the symbolic link"));
        return "NOTHING";
    }

    QFileInfo fi2(tmp);

    if(!fi2.isExecutable()) {
        QMessageBox::critical(nullptr, DISPLAY_NAME " " VERSION,
                              tr("An unexpected error occurred while creating the symbolic link"));
        return "NOTHING";
    } else {
        return tmp;
    }

    return tmp;
}

