//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          appImageHelper
//          Copyright (C) 2020 Ingemar Ceicer
//          http://ceicer.org/ingemar/
//          programmering1 (at) ceicer (dot) org
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3
//   as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#include "widget.h"
#include "ui_widget.h"


void Widget::setRubriker()
{
    QFont font;
    font.setBold(true);
    const QStringList pathToDesktop = QStandardPaths::standardLocations(QStandardPaths::DesktopLocation);
    QStringList rubriker;
    rubriker << tr("Created")
             << tr("Path to AppImage")
             << tr("Path to Icon")
             << tr("Symbolic link")
             << tr("*.desktop file in:\n") + pathToDesktop.at(0)
             << tr("Symbolic link in:\n") + pathToDesktop.at(0);

    foreach(QString s, allApplicationsLocation) {
        QFileInfo fi(s);

        if(fi.exists() && fi.isDir() && fi.isWritable()) {
            rubriker << tr("*.desktop file in:\n") + s;
        }
    }

    for(int i = 0; i < rubriker.size(); i++) {
        QTableWidgetItem *header = new QTableWidgetItem();
        header->setText(rubriker.at(i));
        header->setFont(font);
        ui->tableWidget->insertColumn(i);
        ui->tableWidget->setHorizontalHeaderItem(i, header);
    }
}





