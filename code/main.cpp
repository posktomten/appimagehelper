//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          appImageHelper
//          Copyright (C) 2020 Ingemar Ceicer
//          http://ceicer.org/ingemar/
//          programmering1 (at) ceicer (dot) org
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3
//   as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include "ui_widget.h"
#include <QApplication>
#include <QTranslator>
#include "widget.h"
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QString fontPath = ":/fonts/Ubuntu-R.ttf";
    int fontId = QFontDatabase::addApplicationFont(fontPath);

    if(fontId != -1) {
        QFont f("Ubuntu", LINUX_FONT);
        QApplication::setFont(f);
    }

//        qDebug() << "BRA";
//    } else {
//        qDebug() << "KASS";
//    }
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       "appimagehelper");
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    settings.setIniCodec("UTF-8");
#endif
    settings.beginGroup("Language");
    QString sp = settings.value("language").toString();

    // ipac WORKOROUND
    if(sp == "Svenska") {
        sp = "sv_SE";
    }

    settings.endGroup();

    if(sp == "sv_SE") {
        const QString translationPath = ":/i18n/";
        QTranslator *translator = new QTranslator;

        if(translator->load(translationPath + "complete_" + sp + ".qm")) {
            QApplication::installTranslator(translator);
        }
    }

    Widget *w = new Widget;
    QObject::connect(&a, &QApplication::aboutToQuit,
                     [ w ]() -> void { w->setEndConfig(); });
    w->show();
    return a.exec();
}

