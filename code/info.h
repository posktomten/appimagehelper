//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          appImageHelper
//          Copyright (C) 2020 Ingemar Ceicer
//          http://ceicer.org/ingemar/
//          programmering1 (at) ceicer (dot) org
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3
//   as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#ifndef INFO_H
#define INFO_H

#include <QWidget>
#include <sstream>
#include <string>

// EDIT
//#define HIGH_GLIBC

#ifdef Q_PROCESSOR_X86_32 // 32 bit
#define ZSYNC_ARG_2 "http://bin.ceicer.com/zsync/appimagehelper-i386.AppImage.zsync"
#define ZSYNC_ARG_1 "-i appimagehelper-i386.AppImage"
#endif

#ifndef HIGH_GLIBC
#ifdef Q_PROCESSOR_X86_64 // 64 bit
#define ZSYNC_ARG_2 "http://bin.ceicer.com/zsync/appimagehelper-x86_64.AppImage.zsync"
#define ZSYNC_ARG_1 "-i appimagehelper-x86_64.AppImage"
#endif
#endif

#ifdef HIGH_GLIBC
#ifdef Q_PROCESSOR_X86_64 // 64 bit
#define ZSYNC_ARG_2 "http://bin.ceicer.com/zsync/HIGH_GLIBC/appimagehelper-x86_64.AppImage.zsync"
#define ZSYNC_ARG_1 "-i appimagehelper-x86_64.AppImage"
#endif
#endif

#define VERSION_PATH "http://bin.ceicer.com/appimagehelper/version.txt"
#define VERSION_PATH_SV "http://bin.ceicer.com/appimagehelper/version_sv.txt"
#define DOWNLOAD_PATH                                                          \
    "https://gitlab.com/posktomten/appimagehelper/-/wikis/DOWNLOADS"
#define MANUAL_PATH "https://gitlab.com/posktomten/appimagehelper/-/wikis/Help"
#define DISPLAY_NAME "appImageHelper"
#define VERSION "2.7.4"
#define BUILD_DATE_TIME __DATE__ " " __TIME__
#define START_YEAR "2020"
#define CURRENT_YEAR __DATE__
#define LICENCE_VERSION "3"
#define PROGRAMMER_EMAIL "ic_0002@ceicer.com"
#define PROGRAMMER_NAME "Ingemar Ceicer"
#define PROGRAMMER_PHONE "+46706361747"
#define APPLICATION_HOMEPAGE                                                   \
    "https://gitlab.com/posktomten/appimagehelper/wikis/Home"
#define SOURCEKODE "https://gitlab.com/posktomten/appimagehelper"
#define WIKI "https://gitlab.com/posktomten/appimagehelper/wikis/Home"

#define LINUX_FONT 13

class Info : public QWidget
{
    Q_OBJECT
public:
    void getSystem();

signals:
    void sendValue(bool update);

};

#endif // INFO_H
