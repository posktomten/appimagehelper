/********************************************************************************
** Form generated from reading UI file 'widget.ui'
**
** Created by: Qt User Interface Compiler version 6.4.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WIDGET_H
#define UI_WIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Widget
{
public:
    QVBoxLayout *verticalLayout_7;
    QHBoxLayout *horizontalLayout_2;
    QVBoxLayout *verticalLayout_5;
    QLabel *lblPlats;
    QCheckBox *chChoice;
    QCheckBox *chDesktop;
    QVBoxLayout *verticalLayout;
    QCheckBox *chStandardDesktopFile;
    QCheckBox *chSoftLink;
    QVBoxLayout *VerticalLayout_Shortcut;
    QLabel *lblSelect;
    QComboBox *combSelect;
    QLabel *lblVisaVal;
    QVBoxLayout *verticalLayout_6;
    QHBoxLayout *horizontalLayout;
    QVBoxLayout *verticalLayout_2;
    QPushButton *pbMaintenanceTool;
    QPushButton *pbDoUpdate;
    QPushButton *pbExecite;
    QPushButton *pbQuestion;
    QPushButton *pbAbout;
    QPushButton *pbExit;
    QPushButton *pbCleanUp;
    QVBoxLayout *verticalLayout_3;
    QComboBox *combLanguage;
    QPushButton *pbRemoveShortcuts;
    QPushButton *pbDesktop;
    QPushButton *pbRemoveSelected;
    QPushButton *pbResizeContent;
    QPushButton *pbResizeWidth;
    QVBoxLayout *verticalLayout_4;
    QCheckBox *chDetails;
    QCheckBox *chNewVersion;
    QSpacerItem *verticalSpacer;
    QTableWidget *tableWidget;

    void setupUi(QWidget *Widget)
    {
        if (Widget->objectName().isEmpty())
            Widget->setObjectName("Widget");
        Widget->resize(982, 684);
        verticalLayout_7 = new QVBoxLayout(Widget);
        verticalLayout_7->setObjectName("verticalLayout_7");
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName("horizontalLayout_2");
        verticalLayout_5 = new QVBoxLayout();
        verticalLayout_5->setObjectName("verticalLayout_5");
        lblPlats = new QLabel(Widget);
        lblPlats->setObjectName("lblPlats");

        verticalLayout_5->addWidget(lblPlats);

        chChoice = new QCheckBox(Widget);
        chChoice->setObjectName("chChoice");

        verticalLayout_5->addWidget(chChoice);

        chDesktop = new QCheckBox(Widget);
        chDesktop->setObjectName("chDesktop");
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(chDesktop->sizePolicy().hasHeightForWidth());
        chDesktop->setSizePolicy(sizePolicy);
        chDesktop->setChecked(true);

        verticalLayout_5->addWidget(chDesktop);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName("verticalLayout");
        verticalLayout->setContentsMargins(30, -1, -1, -1);
        chStandardDesktopFile = new QCheckBox(Widget);
        chStandardDesktopFile->setObjectName("chStandardDesktopFile");
        chStandardDesktopFile->setEnabled(true);
        sizePolicy.setHeightForWidth(chStandardDesktopFile->sizePolicy().hasHeightForWidth());
        chStandardDesktopFile->setSizePolicy(sizePolicy);
        chStandardDesktopFile->setChecked(true);

        verticalLayout->addWidget(chStandardDesktopFile);

        chSoftLink = new QCheckBox(Widget);
        chSoftLink->setObjectName("chSoftLink");
        chSoftLink->setEnabled(true);
        sizePolicy.setHeightForWidth(chSoftLink->sizePolicy().hasHeightForWidth());
        chSoftLink->setSizePolicy(sizePolicy);

        verticalLayout->addWidget(chSoftLink);


        verticalLayout_5->addLayout(verticalLayout);

        VerticalLayout_Shortcut = new QVBoxLayout();
        VerticalLayout_Shortcut->setObjectName("VerticalLayout_Shortcut");

        verticalLayout_5->addLayout(VerticalLayout_Shortcut);

        lblSelect = new QLabel(Widget);
        lblSelect->setObjectName("lblSelect");

        verticalLayout_5->addWidget(lblSelect);

        combSelect = new QComboBox(Widget);
        combSelect->setObjectName("combSelect");
        combSelect->setEnabled(true);
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(combSelect->sizePolicy().hasHeightForWidth());
        combSelect->setSizePolicy(sizePolicy1);
        combSelect->setMinimumSize(QSize(200, 0));
        combSelect->setMaximumSize(QSize(200, 30));
        combSelect->setSizeAdjustPolicy(QComboBox::AdjustToContentsOnFirstShow);

        verticalLayout_5->addWidget(combSelect);

        lblVisaVal = new QLabel(Widget);
        lblVisaVal->setObjectName("lblVisaVal");
        QSizePolicy sizePolicy2(QSizePolicy::Fixed, QSizePolicy::Preferred);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(lblVisaVal->sizePolicy().hasHeightForWidth());
        lblVisaVal->setSizePolicy(sizePolicy2);
        lblVisaVal->setMinimumSize(QSize(0, 40));
        lblVisaVal->setMaximumSize(QSize(400, 40));
        lblVisaVal->setFrameShape(QFrame::NoFrame);
        lblVisaVal->setWordWrap(true);

        verticalLayout_5->addWidget(lblVisaVal);


        horizontalLayout_2->addLayout(verticalLayout_5);

        verticalLayout_6 = new QVBoxLayout();
        verticalLayout_6->setObjectName("verticalLayout_6");
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName("horizontalLayout");
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName("verticalLayout_2");
        pbMaintenanceTool = new QPushButton(Widget);
        pbMaintenanceTool->setObjectName("pbMaintenanceTool");
        pbMaintenanceTool->setEnabled(true);

        verticalLayout_2->addWidget(pbMaintenanceTool);

        pbDoUpdate = new QPushButton(Widget);
        pbDoUpdate->setObjectName("pbDoUpdate");
        pbDoUpdate->setEnabled(false);

        verticalLayout_2->addWidget(pbDoUpdate);

        pbExecite = new QPushButton(Widget);
        pbExecite->setObjectName("pbExecite");
        pbExecite->setAutoDefault(false);
        pbExecite->setFlat(false);

        verticalLayout_2->addWidget(pbExecite);

        pbQuestion = new QPushButton(Widget);
        pbQuestion->setObjectName("pbQuestion");
        sizePolicy.setHeightForWidth(pbQuestion->sizePolicy().hasHeightForWidth());
        pbQuestion->setSizePolicy(sizePolicy);
        pbQuestion->setMinimumSize(QSize(0, 0));
        pbQuestion->setMaximumSize(QSize(16777215, 16777215));
        pbQuestion->setFlat(false);

        verticalLayout_2->addWidget(pbQuestion);

        pbAbout = new QPushButton(Widget);
        pbAbout->setObjectName("pbAbout");
        sizePolicy.setHeightForWidth(pbAbout->sizePolicy().hasHeightForWidth());
        pbAbout->setSizePolicy(sizePolicy);
        pbAbout->setMinimumSize(QSize(0, 0));
        pbAbout->setMaximumSize(QSize(16777215, 16777215));
        pbAbout->setFlat(false);

        verticalLayout_2->addWidget(pbAbout);

        pbExit = new QPushButton(Widget);
        pbExit->setObjectName("pbExit");
        pbExit->setEnabled(true);
        sizePolicy.setHeightForWidth(pbExit->sizePolicy().hasHeightForWidth());
        pbExit->setSizePolicy(sizePolicy);
        pbExit->setMinimumSize(QSize(0, 0));
        pbExit->setMaximumSize(QSize(16777215, 16777215));
        pbExit->setFlat(false);

        verticalLayout_2->addWidget(pbExit);

        pbCleanUp = new QPushButton(Widget);
        pbCleanUp->setObjectName("pbCleanUp");
        sizePolicy.setHeightForWidth(pbCleanUp->sizePolicy().hasHeightForWidth());
        pbCleanUp->setSizePolicy(sizePolicy);
        pbCleanUp->setMinimumSize(QSize(0, 0));
        pbCleanUp->setMaximumSize(QSize(16777215, 16777215));
        pbCleanUp->setFlat(false);

        verticalLayout_2->addWidget(pbCleanUp);


        horizontalLayout->addLayout(verticalLayout_2);

        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setObjectName("verticalLayout_3");
        combLanguage = new QComboBox(Widget);
        combLanguage->setObjectName("combLanguage");
        combLanguage->setIconSize(QSize(22, 22));

        verticalLayout_3->addWidget(combLanguage);

        pbRemoveShortcuts = new QPushButton(Widget);
        pbRemoveShortcuts->setObjectName("pbRemoveShortcuts");
        pbRemoveShortcuts->setEnabled(true);
        sizePolicy.setHeightForWidth(pbRemoveShortcuts->sizePolicy().hasHeightForWidth());
        pbRemoveShortcuts->setSizePolicy(sizePolicy);
        pbRemoveShortcuts->setMinimumSize(QSize(0, 0));
        pbRemoveShortcuts->setMaximumSize(QSize(16777215, 16777215));
        pbRemoveShortcuts->setFlat(false);

        verticalLayout_3->addWidget(pbRemoveShortcuts);

        pbDesktop = new QPushButton(Widget);
        pbDesktop->setObjectName("pbDesktop");
        pbDesktop->setEnabled(true);
        sizePolicy.setHeightForWidth(pbDesktop->sizePolicy().hasHeightForWidth());
        pbDesktop->setSizePolicy(sizePolicy);
        pbDesktop->setMinimumSize(QSize(0, 0));
        pbDesktop->setMaximumSize(QSize(16777215, 16777215));
        pbDesktop->setFlat(false);

        verticalLayout_3->addWidget(pbDesktop);

        pbRemoveSelected = new QPushButton(Widget);
        pbRemoveSelected->setObjectName("pbRemoveSelected");
        sizePolicy.setHeightForWidth(pbRemoveSelected->sizePolicy().hasHeightForWidth());
        pbRemoveSelected->setSizePolicy(sizePolicy);
        pbRemoveSelected->setMinimumSize(QSize(0, 0));
        pbRemoveSelected->setMaximumSize(QSize(16777215, 16777215));
        pbRemoveSelected->setFlat(false);

        verticalLayout_3->addWidget(pbRemoveSelected);

        pbResizeContent = new QPushButton(Widget);
        pbResizeContent->setObjectName("pbResizeContent");

        verticalLayout_3->addWidget(pbResizeContent);

        pbResizeWidth = new QPushButton(Widget);
        pbResizeWidth->setObjectName("pbResizeWidth");

        verticalLayout_3->addWidget(pbResizeWidth);


        horizontalLayout->addLayout(verticalLayout_3);


        verticalLayout_6->addLayout(horizontalLayout);

        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setObjectName("verticalLayout_4");
        chDetails = new QCheckBox(Widget);
        chDetails->setObjectName("chDetails");
        chDetails->setMaximumSize(QSize(16777215, 29));

        verticalLayout_4->addWidget(chDetails);

        chNewVersion = new QCheckBox(Widget);
        chNewVersion->setObjectName("chNewVersion");
        QSizePolicy sizePolicy3(QSizePolicy::Preferred, QSizePolicy::Minimum);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(chNewVersion->sizePolicy().hasHeightForWidth());
        chNewVersion->setSizePolicy(sizePolicy3);
        chNewVersion->setMaximumSize(QSize(500, 20));

        verticalLayout_4->addWidget(chNewVersion);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_4->addItem(verticalSpacer);


        verticalLayout_6->addLayout(verticalLayout_4);


        horizontalLayout_2->addLayout(verticalLayout_6);


        verticalLayout_7->addLayout(horizontalLayout_2);

        tableWidget = new QTableWidget(Widget);
        tableWidget->setObjectName("tableWidget");
        QSizePolicy sizePolicy4(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy4.setHorizontalStretch(0);
        sizePolicy4.setVerticalStretch(8);
        sizePolicy4.setHeightForWidth(tableWidget->sizePolicy().hasHeightForWidth());
        tableWidget->setSizePolicy(sizePolicy4);
        tableWidget->setMinimumSize(QSize(0, 0));
        tableWidget->setSizeIncrement(QSize(0, 0));
        tableWidget->setBaseSize(QSize(0, 0));
        tableWidget->setSortingEnabled(true);

        verticalLayout_7->addWidget(tableWidget);


        retranslateUi(Widget);

        QMetaObject::connectSlotsByName(Widget);
    } // setupUi

    void retranslateUi(QWidget *Widget)
    {
        Widget->setWindowTitle(QCoreApplication::translate("Widget", "Widget", nullptr));
        lblPlats->setText(QCoreApplication::translate("Widget", "Decide the location of the shortcut(s).", nullptr));
        chChoice->setText(QCoreApplication::translate("Widget", "Symbolic link (ln -s) in any folder.", nullptr));
        chDesktop->setText(QCoreApplication::translate("Widget", "Shortcut on the desktop (~/Desktop).", nullptr));
        chStandardDesktopFile->setText(QCoreApplication::translate("Widget", "Use Standard *.desktop file (Recommended).", nullptr));
        chSoftLink->setText(QCoreApplication::translate("Widget", "Use Symbolic link (ln -s).", nullptr));
        lblSelect->setText(QCoreApplication::translate("Widget", "Decide the category for your AppImage.", nullptr));
        pbMaintenanceTool->setText(QCoreApplication::translate("Widget", "Search for Update", nullptr));
        pbDoUpdate->setText(QCoreApplication::translate("Widget", "Update", nullptr));
        pbExecite->setText(QCoreApplication::translate("Widget", "Launch AppImage", nullptr));
        pbQuestion->setText(QCoreApplication::translate("Widget", "Help", nullptr));
        pbAbout->setText(QCoreApplication::translate("Widget", "About", nullptr));
        pbExit->setText(QCoreApplication::translate("Widget", "Exit", nullptr));
#if QT_CONFIG(tooltip)
        pbCleanUp->setToolTip(QCoreApplication::translate("Widget", "<html><head/><body><p>Remove missing shortcuts from the database. Red background color.</p></body></html>", nullptr));
#endif // QT_CONFIG(tooltip)
        pbCleanUp->setText(QCoreApplication::translate("Widget", "Remove missing", nullptr));
        pbRemoveShortcuts->setText(QCoreApplication::translate("Widget", "Check your shortcuts", nullptr));
        pbDesktop->setText(QCoreApplication::translate("Widget", "Create Shortcut", nullptr));
        pbRemoveSelected->setText(QCoreApplication::translate("Widget", "Delete selected shortcuts", nullptr));
        pbResizeContent->setText(QCoreApplication::translate("Widget", "Resize to Content", nullptr));
        pbResizeWidth->setText(QCoreApplication::translate("Widget", "No horizontal scrollbar", nullptr));
        chDetails->setText(QCoreApplication::translate("Widget", "View Path.", nullptr));
        chNewVersion->setText(QCoreApplication::translate("Widget", "Do not check for a new version when the program starts.", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Widget: public Ui_Widget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WIDGET_H
