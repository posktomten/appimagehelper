//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          appImageHelper
//          Copyright (C) 2020 Ingemar Ceicer
//          http://ceicer.org/ingemar/
//          programmering1 (at) ceicer (dot) org
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3
//   as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#include "widget.h"
#include "ui_widget.h"
// Första gången
bool Widget::makeDesktopFile(const QString &source, const QString &target, const QString &fileName, const QString &ikon, const QString &comment, const QString &categories)
{
    QString target2 = target;
    target2.append("_" + indextid_sec + ".desktop");
    QFile file(target2);

    if(QFile::exists(target2)) {
        QFile::remove(target2);
    }

    if(file.open(QIODevice::WriteOnly | QIODevice::Text)) {
        QTextStream out(&file);

        if(!ikon.isEmpty()) {
            out << "[Desktop Entry]\nVersion=1.0\nType=Application\nName=" + fileName + ".desktop\nTerminal=false\nIcon=" + ikon + "\nComment=" + comment + "\nExec=\"" + source + "\"\nCategories=" + categories + "\n";
        } else {
            out << "[Desktop Entry]\nVersion=1.0\nType=Application\nName=" + fileName + ".desktop\nTerminal = false\nComment = " + comment + "\nExec = \"" + source + "\"\nCategories=" + categories + "\n";
        }

        file.close();
        file.setPermissions(QFileDevice::ReadUser | QFileDevice::ExeUser |
                            QFileDevice::WriteUser | QFileDevice::ReadOther |
                            QFileDevice::ExeOther);
    } else {
        QMessageBox::critical(
            nullptr, DISPLAY_NAME " " VERSION,
            tr("Failure!\nThe shortcut could not be created.\nCheck your file permissions."));
        return false;
    }

    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       "appimagehelper");
    #if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
  settings.setIniCodec("UTF-8");
#endif
    settings.beginGroup("Categories");
    QString tmp = categories.left(categories.size() - 1);
    QStringList lista;

    if(tmp.contains(';')) {
        int index = tmp.indexOf(';');
        tmp = tmp.left(index);
    }

    lista = settings.value(tmp).toStringList();
    lista << indextid_sec;
    lista.removeDuplicates();
    settings.setValue(tmp, lista);
    settings.endGroup();
    return true;
}


bool Widget::makeDesktopFile2(const QString &source, const QString &target, const QString &fileName, const QString &ikon, const QStringList &kategorier, const QString &sekunder)
{
    QString target2 = target + "/" + fileName;
    target2.append("_" + sekunder + ".desktop");
    QFile file(target2);

    if(QFile::exists(target2)) {
        QFile::remove(target2);
    }

    if(file.open(QIODevice::WriteOnly | QIODevice::Text)) {
        QTextStream out(&file);
        QString kat;

        if(kategorier.size() == 1) {
            int i = items.indexOf(kategorier.at(0));

            if(i == 1) {
                kat = "\nCategories=" + items.at(1) + ";" + items.at(0) + ";\nComment=" + itemsExplained.at(i) + "\n";
            } else if(i == 2) {
                kat = "\nCategories=" + items.at(2) + ";" + items.at(0) + ";\nComment=" + itemsExplained.at(i) + "\n";
            } else {
                kat = "\nCategories=" + items.at(i) + ";\nComment=" + itemsExplained.at(i) + "\n";
            }
        } else {
            kat.append("\nCategories=");

            foreach(QString s, kategorier) {
                kat.append(s + ";");
            }
        }

        if(!ikon.isEmpty()) {
            out << "[Desktop Entry]\nVersion=1.0\nType=Application\nName=" + fileName + ".desktop\nTerminal=false\nIcon=" + ikon  + "\nExec=\"" + source + "\"" + kat;
        } else {
            out << "[Desktop Entry]\nVersion=1.0\nType=Application\nName=" + fileName + ".desktop\nTerminal=false\nExec = \"" + source + "\"" + kat;
        }

        file.close();
        file.setPermissions(QFileDevice::ReadUser | QFileDevice::ExeUser |
                            QFileDevice::WriteUser | QFileDevice::ReadOther |
                            QFileDevice::ExeOther);
    }

    return true;
}
