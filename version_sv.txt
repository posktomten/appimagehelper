2.7.4
Kontrollera gärna på nedladdningssidan om det finns en version som passar dig bättre innan du uppdaterar:<br>https://gitlab.com/posktomten/appimagehelper/-/wikis/DOWNLOADS<br>
Bättre kompatibel med nyare Linux-distributioner som använder GLIBC lika med eller större än 2.35 (2022-02-03). T.ex. Ubuntu 22.04.<br>
Bättre kompatibel med äldre Linux-distributioner som använder GLIBC lika med eller större än 2.27 (2018-02-01). T.ex. Ubuntu 18.04.<br>
Version 2.7.3 för GLIBC lika med eller högre än 2.23 (2016-02-19) är fortfarande tillgänglig för nedladdning. T.ex. för Ubuntu 16.04.
