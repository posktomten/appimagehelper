2.7.4
Please check the download page if there is a version that suits you better before updating:<br>https://gitlab.com/posktomten/appimagehelper/-/wikis/DOWNLOADS<br>
Better compatible with newer Linux distributions using GLIBC equal to or greater than 2.35 (2022-02-03). E.g. Ubuntu 22.04.<br>
Better compatible with older Linux distributions using GLIBC equal to or greater than 2.27 (2018-02-01). E.g. Ubuntu 18.04.<br>
Version 2.7.3 for GLIBC equal to or greater than 2.23 (2016-02-19) are still available for download. E.g. for Ubuntu 16.04.


